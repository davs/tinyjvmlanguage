/**
 * Various support methods are stored here.
 * 
 */

package davs.mini.jvm.lang;

import java.util.HashMap;
import java.util.List;

import org.antlr.runtime.tree.CommonTree;

public class Utils
	{

		public Utils(){}
			
		public Type getTypeFromNode(int node)
			{
				if(node == ProceduralParser.STR || node == ProceduralParser.STRING)
					return Type.STRING;
				else if(node == ProceduralParser.BLN || node == ProceduralParser.BOOLEAN)
					return Type.BOOLEAN;
				else if(node == ProceduralParser.INT || node == ProceduralParser.INTEGER)
					return Type.INTEGER;
				else if ((node) == ProceduralParser.DBL || node == ProceduralParser.DOUBLE)
					return Type.DOUBLE;
				else if(node == ProceduralParser.BLN || node == ProceduralParser.BOOLEAN)
					return Type.INTEGER;
				else return null;
			}
		
		public String getStringFromNode(int node)
			{
				if(node == ProceduralParser.STR || node == ProceduralParser.STRING)
					return "Ljava/lang/String;";
				else if(node == ProceduralParser.INT || node == ProceduralParser.INTEGER)
					return "I";
				else if(node == ProceduralParser.DBL || node == ProceduralParser.DOUBLE)
					return "D";
				else if(node == ProceduralParser.BLN || node == ProceduralParser.BOOLEAN)
					return "I";
				else return null;
			}
		
		public String getStringFromType(Type type)
			{
				if(type == Type.STRING)
					return "Ljava/lang/String;";
				else if(type == Type.INTEGER || type == Type.BOOLEAN)
					return "I";
				else if(type == Type.DOUBLE || type == Type.MIXED)
					return "D";
				else return "V";
			}
		
		public Type getTypeFromString(String value)
			{
				if(value.equals("I")  || value.equals("Z"))
					return Type.INTEGER;
				else if(value.equals("D"))
					return Type.DOUBLE;
				else if(value.equals("Ljava/lang/String;"))
					return Type.STRING;
				else return null;
			}
		
		public boolean areOperandsNumbers(Type type)
			{
				return (type == Type.INTEGER || type == Type.DOUBLE);
			}
		
		public boolean isValidNode(int type)
			{
				if(type == ProceduralParser.EXPRENTER)
					return true;
				if(type == ProceduralParser.EXP)
					return true;
				if(type == ProceduralParser.MULT)
					return true;
				if(type == ProceduralParser.DIV)
					return true;
				if(type == ProceduralParser.MOD)
					return true;
				if(type == ProceduralParser.PLUS)
					return true;
				if(type == ProceduralParser.MINUS)
					return true;
				if(type == ProceduralParser.UNARY)
					return true;
				if(type == ProceduralParser.EQ)
					return true;
				if(type == ProceduralParser.NOTEQ)
					return true;
				if(type == ProceduralParser.LT)
					return true;
				if(type == ProceduralParser.LTEQ)
					return true;
				if(type == ProceduralParser.GT)
					return true;
				if(type == ProceduralParser.GTEQ)
					return true;
				
				return false;
			}
		
		public Type convertDoubleToMixed(Type tmp)
			{
				if(tmp == Type.DOUBLE || tmp == Type.MIXED)
					return Type.MIXED;
				else return Type.INTEGER;
			}
		
		public Type convertIntegerToMixed(Type tmp)
			{
				if(tmp == Type.INTEGER || tmp == Type.MIXED)
					return Type.MIXED;
				else return Type.DOUBLE;
			}
		
		public boolean isCorrectAssignType(Type expectedType, Type receivedType)
			{
				if(expectedType == Type.STRING && receivedType == Type.STRING)
					return true;
				else if(expectedType == Type.DOUBLE && receivedType == Type.DOUBLE)
					return true;
				else if(expectedType == Type.INTEGER && receivedType == Type.INTEGER)
					return true;
				else if(expectedType == Type.DOUBLE && receivedType == Type.INTEGER)
					return true;
				else return false;
			}
		
		public boolean isValidBoolean(Type expectedType, String value)
			{
				return (expectedType == Type.BOOLEAN && isBoolean(value));
			}
		
		private boolean isBoolean(String value)
			{
				if(value.toLowerCase().equals("true") || value.toLowerCase().equals("false") )
					return true;
				return false;
			}
		
		/**
		 * Cycle through nodes and find return type info. (used to check if int to doubleconversion is needed)
		 * If all nodes are integers, return Type.INTEGER, if all nodes are doubles, return Type.DOUBLE,
		 * if there is mix of integers and doubles return Type.MIXED.
		 * 
		 * @param nodes
		 * @param type
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public Type getLoohaheadType(List<CommonTree> nodes, Type type, HashMap<String, Type> fNames, FunctionSpace currentSpace)
			{
				Type tmp = type;
				
				for(CommonTree node : nodes)
					{
						
						if(isValidNode(node.getType()) && node.getChildCount() >= 1)
							{
								if(node.getType() == ProceduralParser.EXP)
									tmp = getLoohaheadType(node.getChildren(), Type.MIXED, fNames, currentSpace);
								tmp = getLoohaheadType(node.getChildren(), tmp, fNames, currentSpace);
							}
						else if(node.getType() == ProceduralParser.INTEGER)
							tmp = convertDoubleToMixed(tmp);
						else if(node.getType() == ProceduralParser.DOUBLE)
							tmp = convertIntegerToMixed(tmp);
						else if(node.getType() == ProceduralParser.BOOLEAN)
							tmp = Type.INTEGER;
						else if(node.getType() == ProceduralParser.STRING)
							tmp = Type.STRING;
						else if(node.getType() == ProceduralParser.VAR_OR_FUNCTCALL)
							{
								Type varOrFunctType = getVarOrFunType(node.getChild(0).getText(),fNames, currentSpace );
								if(varOrFunctType == Type.INTEGER)
									tmp = convertDoubleToMixed(tmp);
								else if(varOrFunctType == Type.DOUBLE)
									tmp = convertIntegerToMixed(tmp);
								else if(varOrFunctType == Type.BOOLEAN)
									return Type.INTEGER;
								else return Type.STRING;
							}
					
					}
				
				return tmp;
			}
		
		private Type getVarOrFunType(String name, HashMap<String, Type> fNames, FunctionSpace currentSpace)
			{
				if(currentSpace.isVarDeclared(name))
					{
						return currentSpace.getLocalType(name);
					}
				else if(fNames.containsKey(name))
					{
						return fNames.get(name);
					}
				else return null;
			}
		
		
		/**
		 * Based on node's type get apropriate return Type.
		 * 
		 * @param type
		 * @return
		 */
		public Type getNodeType(CommonTree node, HashMap<String, Type> fNames, FunctionSpace currentSpace)
			{
				if(node.getType() == ProceduralParser.DOUBLE)
					return Type.DOUBLE;
				else if(node.getType() == ProceduralParser.INTEGER)
					return Type.INTEGER;
				else if(node.getType() == ProceduralParser.STRING)
					return Type.STRING;
				else if (node.getType() == ProceduralParser.VAR_OR_FUNCTCALL)
					{
						Type varOrFunctType = getVarOrFunType(node.getChild(0).getText(), fNames, currentSpace);
						if(varOrFunctType == Type.INTEGER)
							return Type.INTEGER;
						else if(varOrFunctType == Type.DOUBLE)
							return Type.DOUBLE;
						else return Type.STRING;
					}
				else return Type.INVALID;
				
			}
		
	}
