/**
 * Internal representation of constructs that use logical operators (>,>=,<,<=,==,<>)
 */

package davs.mini.jvm.lang;

public enum LogicConstructs
	{
		WHILE, IFELSE
	}
