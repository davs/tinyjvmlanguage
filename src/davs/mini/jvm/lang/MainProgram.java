package davs.mini.jvm.lang;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.antlr.runtime.RecognitionException;

import COM.sootNsmoke.oolong.Oolong;

public class MainProgram
	{
		static boolean compileErrorStatus;
		
		public static void main(String[] args) throws Exception
			{
				
				String fileName = null;
				String instructions = null;
				compileErrorStatus = false;
				
				if(args.length == 1 || args.length == 2)
					{
						fileName = args[0];
						
						String oolongFilePath = "./" + getFileNameNoExtension(fileName);
						String[] oolongArgs = new String [1];
						oolongArgs[0] = oolongFilePath;
						
						//oolong source file will be placed in the current folder
						File oolongFile = new File(oolongFilePath);
						
						if(!fileName.endsWith(".mini"))
							{
								System.out.println("File must have valid extension of .mini");
								return;
							}
						
						instructions = getInstructions(fileName);
						
						if(compileErrorStatus)
							return;
						
						oolongFile.createNewFile();
						BufferedWriter out = new BufferedWriter(new FileWriter(oolongFilePath));
						out.write(instructions);
						
						out.close();
						
						//Instructions are now written in oolong source file
						//All that is left to do, is to use oolong assembler to create class file
						Oolong.main(oolongArgs);
						oolongFile.delete();
						
					}
				else System.out.println("Source file not found!");
				
				
				if(args.length == 2)
					{
						if(args[1].equals("-i") || args[1].equals("-I"))
								System.out.println(instructions);
						else System.out.println("Use -i or -I switch to view generated instructions");
					}
				
			}
		
		public static String getInstructions(String fileName) throws IOException, RecognitionException
			{
				
				Interpreter interp = new Interpreter();
				String fileContents = readFileToString(fileName);
				interp.interp(fileContents, getFileNameNoExtension(fileName));
				String instructions = interp.getInstructions();
				
				boolean hasCompileErrors = interp.getCompileStatus();
				
				if(hasCompileErrors)
					{
						compileErrorStatus = true;
						return null;
					}
				else return instructions;
			
			}
		
		
		public static String readFileToString(String filePath) throws IOException
			{
				
				String nl = System.getProperty("line.separator");
				StringBuffer fileData = new StringBuffer(1000);
				BufferedReader reader = new BufferedReader(new FileReader(filePath));
				char[] buf = new char[1024];
				int numRead = 0;

				while ((numRead = reader.read(buf)) != -1)
					{
						fileData.append(buf, 0, numRead);
					}
				reader.close();
				fileData.append(nl); //Extra newline is added at the end of a file
				return fileData.toString();
			
			}
		
		public static String getFileNameNoExtension(String fileName)
			{
				return fileName.substring(0, fileName.length()-5);
			}
		
	}
