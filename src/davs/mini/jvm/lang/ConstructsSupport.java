/**
 * Class containing all data necesary to support logical constructs (if-else, while)
 */

package davs.mini.jvm.lang;

import java.util.Stack;

public class ConstructsSupport
	{
		private Stack<LogicConstructs> logicConstructs;
		private WhileLoop whileLoopSupport;
		private IfElse ifElse;
		
		public ConstructsSupport()
			{
				logicConstructs = new Stack<LogicConstructs>();
				whileLoopSupport = new WhileLoop();
				ifElse = new IfElse();
			}
		
		public void pushIfElse(Boolean isElsePresent)
			{
				logicConstructs.push(LogicConstructs.IFELSE);
				ifElse.pushIfElse(isElsePresent);
			}
		
		public void pushWhile()
			{
				logicConstructs.push(LogicConstructs.WHILE);
				whileLoopSupport.enterWhile();
			}
		
		public LogicConstructs getCurrentConstruct()
			{
				return logicConstructs.peek();
			}
		
		public void popWhile()
			{
				logicConstructs.pop();
				whileLoopSupport.popWhile();
			}
		
		public int getWhileMagicNumber()
			{
				return whileLoopSupport.getWhileMagicNumber();
			}
		
		public int getIfElseMagicNumber()
			{
				return ifElse.getIfElseMagicNumber();
			}
		
		public void popIfElse()
			{
				ifElse.popIfElse();
			}
		
		public boolean isElsePresent()
			{
				return ifElse.isElsePresent();
			}

	}
