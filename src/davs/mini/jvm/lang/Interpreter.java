/**
 * Walk AST here and produce appropriate instructions.
 */

package davs.mini.jvm.lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;

public class Interpreter
	{
		
		private ErrorReporter reportError; //Error reporting utility
		private InstructionsWriter writeInstruction; //Object that generates textual instructions
		private Utils utils; //Helper methods
		private boolean compileError; //If true, compile process will fail
		private boolean isNextOperandUnary; //If true, flip sign on next operand
		private boolean convertNextInteger; //If true, convert integers to doubles
		private Type currentType;
		
		private Stack<FunctionSpace> functionSpace; //Current memory model is flat, used stack for future
		
		//Following hash tables are currently global, should be moved to function space if memory model changes
		private HashMap<String, Type> functionNames; //List of function names and their return types
		private HashMap<String, Integer> functionNoParams; //Number of params for each function
		private HashMap<String, List<Type>> functionParamTypes; //Store param types
		
		private boolean isStaticMainPrinted; //If true, function declarations have ended and static main has been outputed to the bytecode
		
		private CommonTree root;
		private TokenRewriteStream tokens;
		private ProceduralLexer lex;
		private ProceduralParser parser;


		/**
		 * Setup method for the interpreter. Based on input stream it creates
		 * lexer, parser and AST, and it starts walking AST.
		 * 
		 * @param input Input stream.
		 * @throws RecognitionException
		 * @throws IOException
		 */
		public void interp(String input, String fileName) throws RecognitionException, IOException
			{

				try
					{

						// lex = new ProceduralLexer(new ANTLRInputStream(input)); <<-- Case sensitive input
						lex = new ProceduralLexer(new NoCase(input)); // Case insensitive input
						tokens = new TokenRewriteStream(lex);
						parser = new ProceduralParser(tokens);
						
						reportError = new ErrorReporter();
						writeInstruction = new InstructionsWriter(fileName);
						utils = new Utils();
						setCompileError(false);
						currentType = Type.INVALID;
						
						functionSpace = new Stack<FunctionSpace>();
						functionNames = new HashMap<String, Type>();
						functionParamTypes = new HashMap<String, List<Type>>();
						functionNoParams = new HashMap<String, Integer>();
						
						isStaticMainPrinted = false;
						isNextOperandUnary = false;
						convertNextInteger = false;

						ProceduralParser.program_return r = parser.program();

						if (parser.getNumberOfSyntaxErrors() != 0)
							{
								setCompileError(true);
								reportError.synatxErrors();
							}
						else
							{
								root = (CommonTree) r.getTree();
								block(root);
							}

					} catch (IOException e)
					{
						reportError.sourceNotFound();
					} catch (RecognitionException e)
					{
						reportError.synatxErrors();
					} catch (Exception e)
					{
						System.out.println(e.getStackTrace());
						reportError.exitedWithError(); // Something has gone very wrong
					}

			}// end interp()

		
		/**
		 * Grab all children from given node, and execute them all one by one by calling exec method.
		 * If node after BLOCK node is not FUNCT node, all functions have been declared. (if they exists at all)
		 * 
		 * @param node Node to be inspected and/or executed.
		 */
		@SuppressWarnings("unchecked")
		// Suppressed because only CommonTrees are retrieved by getChildren().
		private void block(CommonTree node)
			{
				if(!isStaticMainPrinted)
					{
						if(node.getChild(0).getType() == ProceduralParser.BLOCK)
							{
								isStaticMainPrinted = true;
								writeInstruction.startMain();
								enterFunction();
							}
					}
				

				List<CommonTree> stats = node.getChildren();

				for (CommonTree nodeToExec : stats)
					exec(nodeToExec);

			} // end block

		
		/**
		 * Method that is basically giant switch statement which inspects given node,
		 * determines node's type and triggers method designated for particular node's type.
		 * 
		 * @param node Node to be inspected and/or executed.
		 * @return Returned value from single tree type.
		 */
		private Object exec(CommonTree node)
			{

				try
					{

// *************************************************************************************************************************************
// ************************************************* C O R E		 F U N C T I O N A L I T Y *****************************************
// *************************************************************************************************************************************

					switch (node.getType())
						{
												
							case ProceduralParser.BLOCK:
								block(node);
								break;

							case ProceduralParser.DUMMY:
								break; // Do nothing on dummy node
								
							case ProceduralParser.FUNCT:
								functionDeclaration(node);
								break;
								
							case ProceduralParser.CALLVOID:
								if(isFunctionVoid(node.getChild(0).getText()))
									functionCall(node);
								else
									{
										reportError.callSub(node.getChild(0).getText(), node.getLine());
										setCompileError(true);
										return null;
									}
								break;

							case ProceduralParser.ASSIGN:
								assign(node);
								break;

							case ProceduralParser.VARDECLARE:
								String name = node.getChild(0).getText();
								Type type = utils.getTypeFromNode(node.getChild(1).getType());
								declareVar(name, type);
								break;
								
							case ProceduralParser.WHILE:
								whileLoop(node);
								break;
								
							case ProceduralParser.IFELSE:
								ifElseStatement(node);
								break;

// *************************************************************************************************************************************
// ************************************************* B U I L T - I N 	F U N C T I O N S **********************************************
// *************************************************************************************************************************************

							case ProceduralParser.PRINT:
								print(node);
								break;

// *************************************************************************************************************************************
// ************************************************* E X P R E S S I O N S *************************************************************
// *************************************************************************************************************************************

								
// ----------------------------------------------------- P R I M I T I V E S -----------------------------------------------------------
								
								
							case ProceduralParser.DOUBLE:
								Double d = null;
								
								try
									{
										d = Double.parseDouble(node.getText());
										
										if(isNextOperandUnary)
											{
												writeInstruction.pushValue(-d, Type.DOUBLE);
												isNextOperandUnary = false;
											}
										else writeInstruction.pushValue(d, Type.DOUBLE);
									}
								catch(NumberFormatException e)
									{
										reportError.invalidValue(node.getText(), node.getLine());
										setCompileError(true);
									}
								break;
									
								
							case ProceduralParser.INTEGER:
								Integer i = null;
								
								try
									{
										i = Integer.parseInt(node.getText());
										
										if(isNextOperandUnary)
											{
												writeInstruction.pushValue(-i, Type.INTEGER);
												isNextOperandUnary = false;
											}
										else writeInstruction.pushValue(i, Type.INTEGER);
										
										if(convertNextInteger)
											writeInstruction.i2d();
									}
								catch(NumberFormatException e)
									{
										reportError.invalidValue(node.getText(), node.getLine());
										setCompileError(true);
									}
								break;
								
								
							case ProceduralParser.STRING:
								
								if(isNextOperandUnary)
									{
										reportError.incompatibleDataType(node.getLine());
										setCompileError(true);
										return null;
									}
								
								String s = node.getText().substring(1, node.getText().length() - 1);
								writeInstruction.pushValue(s, Type.STRING);
								break;
								
								
							case ProceduralParser.BOOLEAN:
								
								if(isNextOperandUnary)
									{
										reportError.incompatibleDataType(node.getLine());
										setCompileError(true);
										return null;
									}
								
								String b = node.getText().toLowerCase();
								
								if(b.equals("true"))
									writeInstruction.pushValue(1, Type.BOOLEAN);
								else if (b.equals("false"))
									writeInstruction.pushValue(0, Type.BOOLEAN);
								else
									{
										reportError.invalidValue(b, node.getLine());
										setCompileError(true);
									}
								
								break;
								
							case ProceduralParser.VAR_OR_FUNCTCALL:
								//One chile means var call, more than one function call
								if(node.getChildCount() > 1)
									functionCall(node);
								else resolveVar(node.getChild(0).getText(), node.getLine()); 
								break;
								
							case ProceduralParser.EXPRENTER:
								block(node);
								break;
								
								
// ----------------------------------------------------- A R I T H M E T I C S ---------------------------------------------------------		
								
								
							case ProceduralParser.EXP:
								arithmetic(node);
								break;
								
							case ProceduralParser.UNARY:
								unary(node);
								break;
								
							case ProceduralParser.MULT:
								arithmetic(node);
								break;
								
							case ProceduralParser.DIV:
								arithmetic(node);
								break;
								
							case ProceduralParser.MOD:
								arithmetic(node);
								break;
								
							case ProceduralParser.PLUS:
								arithmetic(node);
								break;

								
// -------------------------------------------------- L O G I C A L   O P E R A T O R S ------------------------------------------------	
							
							case ProceduralParser.MINUS:
								arithmetic(node);
								break;
								
							case ProceduralParser.EQ:
								logic(node);
								break;
								
							case ProceduralParser.NOTEQ:
								logic(node);
								break;
								
							case ProceduralParser.LT:
								logic(node);
								break;
								
							case ProceduralParser.LTEQ:
								logic(node);
								break;
								
							case ProceduralParser.GTEQ:
								logic(node);
								break;
								
							case ProceduralParser.GT:
								logic(node);
								break;
								
							default:
								throw new UnsupportedOperationException("Node "	+ node.getText() + "<" + node.getType() + "> not handled");
							}

					} catch (Exception e)
					{ System.err.println("Program exited with error!"); System.out.println(e.getStackTrace());} // Something has gone very wrong 

				return null;

			}// end exec()

		
		
		
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// **************************************************** I M P L E M E N T A T I O N ****************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
		
		
		
// *************************************************************************************************************************************
// **************************************************** C O R E 	F U N C T I O N A L I T Y ******************************************
// *************************************************************************************************************************************


		
		/**
		 * Output function declaration code into the bytecode. Two dummy nodes serve to check if return value and
		 * parameters are present. I let oolong to take care of limit locals/stack directives (should be probably 
		 * optimized in a more serious program)
		 * 
		 * @param node Node holding function's name, parameters and return value (if any), and function's body 
		 */
		private void functionDeclaration(CommonTree node)
			{
				enterFunction();
				
				//Function's info will be stored in the following objects
				Type returnType = null; //Assume function's return type is void by default
				String returnValueType = "V"; //"V" is instruction for default void return
				String functionName = "";
				String params = ""; //Assume no params
				
				//Index location for slist and function name
				short functNameID = 1;
				short slistID = 2;
				short returnID = 0; //Assume function returns void
				
				//Get return value string, if there is one
				if(node.getChild(0).getType() != ProceduralParser.DUMMY)
					{
						returnValueType = utils.getStringFromNode(node.getChild(0).getType());
						returnType = utils.getTypeFromString(returnValueType);
						functNameID = 2;
						slistID = 3;
					}
				
				//Get function's name
				functionName = node.getChild(functNameID).getText();
				
				//Check for duplicate function names
				if(isFunctionDeclared(functionName))
					{
						reportError.duplicateDeclaration(functionName);
						setCompileError(true);
						return;
					}
				else declareFunction(functionName, returnType);
				
				int lastDummyIndex = slistID + 1; //Get location of last DUMMY node
				
				//Check if function has return value
				if(node.getChild(slistID+1).getType() != ProceduralParser.DUMMY)
					{
						returnID = (short) (slistID+1);
						lastDummyIndex++;
					}
				
				List<Type> paramTypes = new ArrayList<Type>();
				
				Integer paramNumbers = 0;// Number of parameters in the function
				
				//Get params if they exist. Cycle in pairs of two getting type and name values
				if(node.getChild(node.getChildCount()-1).getType() != ProceduralParser.DUMMY)
					{
						int lastID = node.getChildCount()-1; //Last node index
						int i, j;//i gets type, j gets name param value
						String varName = "";
						Type type;
						
						for(i=lastDummyIndex+1, j=lastDummyIndex+2; j<=lastID; i=i+2,j=j+2)
							{
								params = params + utils.getStringFromNode(node.getChild(i).getType());
								
								varName = node.getChild(j).getText().toLowerCase();
								type = utils.getTypeFromNode(node.getChild(i).getType());
								
								saveInCurrentSpace(varName, type);
								
								paramNumbers++;
								paramTypes.add(type);
							}
						//Save params list for further validation
						saveFunctionParamTypes(functionName, paramTypes);
					}
				
				saveFunctionNoParams(functionName, paramNumbers);
	
				//Check values for null
				if(returnValueType == null || functionName==null)
					{
						reportError.incompatibleDataType(node.getLine());
						setCompileError(true);
						return;
					}
				
				//If function returns void, there can not be return statement...
				if(returnValueType.equals("V") && returnID != 0)
					{
						reportError.voidFunctNoReturn(functionName, node.getLine());
						setCompileError(true);
						return;
					}
				
				//...and vice-versa
				if(!returnValueType.equals("V") && returnID == 0)
					{
						reportError.voidFunctNoReturn(functionName, node.getLine());
						setCompileError(true);
						return;
					}
				
				//Write bytecodes, first method start
				writeInstruction.methodStart(functionName, params, returnValueType);

				//Next, execute method body
				exec( (CommonTree) node.getChild(slistID) );
				
				//Finally, check that last value on the stack and return value match
				if(returnID != 0)
					{
						List<CommonTree> tmp = new LinkedList<CommonTree>();
						tmp.add((CommonTree) node.getChild(returnID));
						execExpression(tmp, (CommonTree) node.getChild(returnID));
					}
				
				//Push correct return statement and close function body
				writeInstruction.returnFromMethod(returnValueType);	
				writeInstruction.methodEnd();

				exitFunction();

			}
		
		/**
		 * Handles assignment x = y, logical x=y is handled in logic method. 
		 * 
		 * @param node Node containing variable's name (x) and value (y).
		 */
		private void assign(CommonTree node)
			{
				
				String name = node.getChild(0).getText();
				
				if(!isVarDeclared(name))
					{
						reportError.noSuchVar(name, node.getLine());
						setCompileError(true);
						return;
					}
				
				//Get var's type and index
				Type expectedType = getVarType(name);
				Integer index = getVarIndex(name);
				
				List<CommonTree> expr = new LinkedList<CommonTree>();
				expr.add((CommonTree) node.getChild(1));
						
				//Now, all that is left to do is to execute expression associated with
				//variable's name and check that it matches declared type
				Type receivedType = execExpression(expr, (CommonTree) node.getChild(1));

				
				if(utils.isValidBoolean(expectedType, node.getChild(1).getText()))
					writeInstruction.saveVariable(expectedType, index);
				else if(!utils.isCorrectAssignType(expectedType, receivedType))
					{
						reportError.incompatibleDataType(node.getLine());
						setCompileError(true);
					}
				else writeInstruction.saveVariable(expectedType, index);
					
			}
		
		
		/**
		 * While loop logic, get guard expression and execute loop's statement list
		 */
		private void whileLoop(CommonTree node)
			{
				enterWhileLoop();
				
				int magicNumber = getMagicNumber(); //Uniquely identifies each label
				
				//Start of the loop is marked, so it can be recalled with goto command
				writeInstruction.startWhile(magicNumber);
				
				//Now, write guard (always child 0)
				//Here, I'm reusing already written function for executing expressions,
				//which will convert all logic operators to either ints or to doubles (ifeq or icmpeq)
				List<CommonTree> guard = new LinkedList<CommonTree>();
				guard.add((CommonTree) node.getChild(0));
				execExpression(guard, (CommonTree) node.getChild(0));
				
				//Next, write loop's statement body (always child 1)
				exec((CommonTree) node.getChild(1));

				exitWhileLoop();
				
				//Goto start of loop for guard reevulation
				writeInstruction.gotoStartWhile(magicNumber);

				//Print label to be able to break out of loop
				writeInstruction.endWhile(magicNumber);
				
			}
		
		
		/**
		 * If-else logic is handled here
		 */
		private void ifElseStatement(CommonTree node)
			{
				boolean isElsePresent = false;
				
				if(node.getChildCount() == 3)
					isElsePresent = true;
				
				enterIfElse(isElsePresent);
				
				int magicNumber = getMagicNumber();
				
				//Write guard
				List<CommonTree> guard = new LinkedList<CommonTree>();
				guard.add((CommonTree) node.getChild(0));
				execExpression(guard, (CommonTree) node.getChild(0));
				
				//Exec main slist
				exec((CommonTree) node.getChild(1));
				writeInstruction.gotoIfEnd(magicNumber);
				
				//If there is else block, write it
				if(isElsePresent)
					{
						writeInstruction.printElse(magicNumber);
						exec((CommonTree) node.getChild(2));
					}
				
				writeInstruction.endIfElse(magicNumber);
				
				exitIfElse();
				
			}
		
		
		
		
// *************************************************************************************************************************************
// **************************************************** B U I L T - I N 	F U N C T I O N S ******************************************
// *************************************************************************************************************************************
		
		
		
		/**
		 * Simple print command.
		 * 
		 * @param node Node holding expression to be printed
		 */
		@SuppressWarnings("unchecked") //Only CommonTree nodes are children
		private void print(CommonTree node)
			{
				
				writeInstruction.stagePrint(); //setup print command
				
				String printedType = getPrintType(node.getChildren(), (CommonTree) node.getChild(0));

				if(printedType == null)
					{
						reportError.incompatibleDataType(node.getLine());
						setCompileError(true);
						return;
					}
				
				writeInstruction.print(printedType); //print actual value
				
			}
		
		
		
// *************************************************************************************************************************************
// ****************************************************	E X P R E S S I O N S **********************************************************
// *************************************************************************************************************************************

		
		
		//TODO look into rewriting this as void method, or breaking it up, use feels awkward
		/**
		 * First look ahead in expression tree to see if there is need for integer to double conversion,
		 * then check if next integer is negative. Finally, execute specific expression node.
		 * 
		 * @param exprTree Expression tree that needs to be validated
		 * @param expr Specific node that is actually being executed
		 */
		private Type execExpression(List<CommonTree> exprTree, CommonTree expr)
			{
				Type receivedType = getLoohaheadType(exprTree);
				
				if(receivedType == Type.MIXED)
					{
						convertNextInteger = true;
						currentType = Type.DOUBLE;
					}
				else currentType = receivedType;
				
				if(receivedType == Type.DOUBLE && receivedType == Type.INTEGER)
					convertNextInteger = true;
				
				exec(expr);
				convertNextInteger = false;
				
				return currentType;
			}
		
		
		/**
		 * Cases such as x = y. Only y is resolved in this method.
		 * 
		 * @param name variable's name
		 */
		private void resolveVar(String name, int lineNo)
			{

				if(!isVarDeclared(name))
					{
						reportError.noSuchVar(name,lineNo);
						setCompileError(true);
						return;
					}
				
				Type type = getVarType(name);
				Integer index = getVarIndex(name);
				writeInstruction.loadVariable(type, index);
				
				if(isNextOperandUnary && utils.areOperandsNumbers(type))
					{
						if(type == Type.INTEGER)
							writeInstruction.negateInteger();
						else writeInstruction.negateDouble();
						isNextOperandUnary = false;
					}
				else if (isNextOperandUnary)
					{
						reportError.incompatibleDataType(lineNo);
						setCompileError(true);
						return;
					}
				
				if(type == Type.INTEGER && convertNextInteger)
					writeInstruction.i2d();
					
			}
		
		
		/**
		 * Perform function call.
		 * 
		 * @param node Node containing function's name and arguments if they exist
		 */
		private void functionCall(CommonTree node)
			{
				
				String name = node.getChild(0).getText();
				
				//Check if function is declared
				if(!isFunctionDeclared(name))
					{
						reportError.noSuchFunction(name, node.getLine());
						setCompileError(true);
						return;
					}
				
				Type returnType = getRetrunType(name);

				//Get index numbers for args
				int startIndex = 2;
				int endIndex = startIndex;
				
				//First check if overall number of params/args matches
				Integer declaredParams = getNoParams(name);
				Integer argNumber = node.getChildCount() - 2; //Subtract name and lparen tokens
				
				if(declaredParams != argNumber)
					{
						reportError.argsMissmatch(name, node.getLine());
						setCompileError(true);
						return;
					}
				
				String args = "";
				
				//If there are no params, there is no need to cycle through args
				if(node.getChildCount() > 2)
					{
						endIndex = node.getChildCount() -1;
						
						//All arg types will be stored here for execution
						List<Type> argTypes = new ArrayList<Type>();
						
						//All arg recieved arg types will be stored here for arg type checks
						List<Type> checks = new ArrayList<Type>();
						
						//Save arg in list, then execute them one by one. This pushes them on the call stack
						for(int i=startIndex; i<=endIndex; i++)
							{
								argTypes.add(getNodeType((CommonTree) node.getChild(i)));

								List<CommonTree> toExec = new LinkedList<CommonTree>();
								toExec.add((CommonTree) node.getChild(i));
								
								//tmp is recieved type
								Type tmp = execExpression(toExec, (CommonTree) node.getChild(i));
								checks.add(tmp);
								
							}
						
						//Now get param types (types that were declared at function declaration)
						List<Type> paramTypes = getParamTypes(name);
						
						for(int i=0; i < paramTypes.size(); i++)
							{
								Type tmp = checks.get(i);
								
								if(tmp != paramTypes.get(i))
									{
										//Allow exception for int2double conversions
										if(tmp == Type.INTEGER && paramTypes.get(i) == Type.DOUBLE)
											tmp = Type.DOUBLE;
										else
											{
												reportError.typeMissmatch(name, node.getLine());
												setCompileError(true);
												return;
											}
									}
								
								args = args + utils.getStringFromType(tmp);
								
							}
					}
				
				String returnValue = utils.getStringFromType(returnType);
				writeInstruction.methodCall(name, args, returnValue);
				
			}
		
		
		
		
// ----------------------------------------------------------- A R I T H M E T I C S ---------------------------------------------------		
		
		

		/**
		 * Handle exponent, multiplication, division, modulo, addition and subtraction here.
		 * 
		 * However, first go forward in execution and confirm that all nodes are compatible (i.e. no adding
		 * of integers and string), and check if nodes need conversion (i.e. i2d)
		 * 
		 * @param node
		 */
		private void arithmetic(CommonTree node)
			{
				
				exec((CommonTree) node.getChild(0));
				exec((CommonTree) node.getChild(1));
				
				switch (node.getType())
					{
						
						case ProceduralParser.EXP:
							if(currentType == Type.DOUBLE || currentType == Type.INTEGER)
								writeInstruction.exponent();
							else
								{
									reportError.incompatibleDataType(node.getLine());
									setCompileError(true);
									return;
								}
							break;
	
						case ProceduralParser.MULT:
							if(currentType == Type.INTEGER)
								writeInstruction.multIntegers();
							else if(currentType == Type.DOUBLE)
								writeInstruction.multDoubles();
							else
								{
									reportError.incompatibleDataType(node.getLine());
									setCompileError(true);
									return;
								}
							break;
							
						//TODO WRAP DIV/MOD BY ZERO MESAGE	
						case ProceduralParser.DIV:
							if(currentType == Type.INTEGER)
								writeInstruction.divIntegers();
							else if(currentType == Type.DOUBLE)
								writeInstruction.divDoubles();
							else
								{
									reportError.incompatibleDataType(node.getLine());
									setCompileError(true);
									return;
								}
							break;
							
						case ProceduralParser.MOD:
							if(currentType == Type.INTEGER)
								writeInstruction.modIntegers();
							else if(currentType == Type.DOUBLE)
								writeInstruction.modDoubles();
							else
								{
									reportError.incompatibleDataType(node.getLine());
									setCompileError(true);
									return;
								}
							break;
							
						case ProceduralParser.PLUS:
							if(currentType == Type.INTEGER)
								writeInstruction.addIntegers();
							else if(currentType == Type.DOUBLE)
								writeInstruction.addDoubles();
							else
								{
									reportError.incompatibleDataType(node.getLine());
									setCompileError(true);
									return;
								}
							break;
							
						case ProceduralParser.MINUS:
							if(currentType == Type.INTEGER)
								writeInstruction.subIntegers();
							else if(currentType == Type.DOUBLE)
								writeInstruction.subDoubles();
							else
								{
									reportError.incompatibleDataType(node.getLine());
									setCompileError(true);
									return;
								}
							break;
						
					}
				
			}
		
		
		/**
		 * Handle unary minus here. First part of main if statement deals with cases such as:
		 * - (- x), else part deals with ordinary -x.
		 * 
		 * @param node
		 */
		private void unary(CommonTree node)
			{
				if(isNextOperandUnary == true)
					{
						exec( (CommonTree) node.getChild(0) );
						
						if(currentType == Type.INTEGER)
							writeInstruction.negateInteger();
						else writeInstruction.negateDouble();
					}
				else
					{
						isNextOperandUnary = true;
						exec( (CommonTree) node.getChild(0) );
					}
					
			}
		

		
		
// ----------------------------------------------------------- L O G I C A L   O P E R A T O R S ---------------------------------------		

		
		
		private void logic(CommonTree node)
			{
				
				//Execute both operands.
				exec( (CommonTree) node.getChild(0) );
				exec( (CommonTree) node.getChild(1) );
				
				int magicNumber = getMagicNumber();
				
				
				//Check opposite condition first, and if true, break out of loop
				
				switch (node.getType())
				{  
					
					case ProceduralParser.EQ:
							
						if(getCurrentConstruct() == LogicConstructs.WHILE)
							{
								if(currentType == Type.DOUBLE)
									writeInstruction.ifNotEqualQuitWhile(magicNumber);
								else writeInstruction.ifIntNotEqualQuitWhile(magicNumber);
							}
						
						if(getCurrentConstruct() == LogicConstructs.IFELSE)
							{
								if(currentType == Type.DOUBLE && isElsePresent())
									writeInstruction.ifNotEqualGotoElse(magicNumber);
								else if(currentType == Type.DOUBLE)
									writeInstruction.ifNotEqualQuitIfStat(magicNumber);
								else if(currentType == Type.INTEGER && isElsePresent())
									writeInstruction.ifIntNotEqualGotoElse(magicNumber);
								else writeInstruction.ifIntNotEqualQuitIfStat(magicNumber);
							}
						
						break;
						
					case ProceduralParser.NOTEQ:
						
						if(getCurrentConstruct() == LogicConstructs.WHILE)
							{
								if(currentType == Type.DOUBLE)
									writeInstruction.ifEqualQuitWhile(magicNumber);
								else writeInstruction.ifIntEqualQuitWhile(magicNumber);
							}
						
						if(getCurrentConstruct() == LogicConstructs.IFELSE)
							{
								if(currentType == Type.DOUBLE && isElsePresent())
									writeInstruction.ifEqualGotoElse(magicNumber);
								else if(currentType == Type.DOUBLE)
									writeInstruction.ifEqualQuitIfStat(magicNumber);
								else if(currentType == Type.INTEGER && isElsePresent())
									writeInstruction.ifIntEqualGotoElse(magicNumber);
								else writeInstruction.ifIntEqualQuitIfStat(magicNumber);
							}
						
						break;
						
					case ProceduralParser.LT:
						
						if(getCurrentConstruct() == LogicConstructs.WHILE)
							{
								if(currentType == Type.DOUBLE)
									writeInstruction.ifGreaterOrEqualQuitWhile(magicNumber);
								else writeInstruction.ifIntGreaterOrEqualQuitWhile(magicNumber);
							}
						
						if(getCurrentConstruct() == LogicConstructs.IFELSE)
							{
								if(currentType == Type.DOUBLE && isElsePresent())
									writeInstruction.ifGreaterOrEqualGotoElse(magicNumber);
								else if(currentType == Type.DOUBLE)
									writeInstruction.ifGreaterOrEqualQuitIfStat(magicNumber);
								else if(currentType == Type.INTEGER && isElsePresent())
									writeInstruction.ifIntGreaterOrEqualGotoElse(magicNumber);
								else writeInstruction.ifIntGreaterOrEqualQuitIfStat(magicNumber);
							}
						
						break;
						
					case ProceduralParser.LTEQ:
						
						if(getCurrentConstruct() == LogicConstructs.WHILE)
							{
								if(currentType == Type.DOUBLE)
									writeInstruction.ifGreaterQuitWhile(magicNumber);
								else writeInstruction.ifIntGreaterQuitWhile(magicNumber);
							}
						
						if(getCurrentConstruct() == LogicConstructs.IFELSE)
							{
								if(currentType == Type.DOUBLE && isElsePresent())
									writeInstruction.ifGreaterGotoElse(magicNumber);
								else if(currentType == Type.DOUBLE)
									writeInstruction.ifGreaterQuitIfStat(magicNumber);
								else if(currentType == Type.INTEGER && isElsePresent())
									writeInstruction.ifIntGreaterGotoElse(magicNumber);
								else writeInstruction.ifIntGreaterQuitIfStat(magicNumber);
							}
						
						break;
						
					case ProceduralParser.GTEQ:
						
						if(getCurrentConstruct() == LogicConstructs.WHILE)
							{
								if(currentType == Type.DOUBLE)
									writeInstruction.ifLessQuitWhile(magicNumber);
								else writeInstruction.ifIntLessQuitWhile(magicNumber);
							}
						
						if(getCurrentConstruct() == LogicConstructs.IFELSE)
							{
								if(currentType == Type.DOUBLE && isElsePresent())
									writeInstruction.ifLessGotoElse(magicNumber);
								else if(currentType == Type.DOUBLE)
									writeInstruction.ifLessQuitIfStat(magicNumber);
								else if(currentType == Type.INTEGER && isElsePresent())
									writeInstruction.ifIntLessGotoElse(magicNumber);
								else writeInstruction.ifIntLessQuitIfStat(magicNumber);
							}
						
						break;
					
					case ProceduralParser.GT:
						
						if(getCurrentConstruct() == LogicConstructs.WHILE)
							{
								if(currentType == Type.DOUBLE)
									writeInstruction.ifLessOrEqualQuitWhile(magicNumber);
								else writeInstruction.ifIntLessOrEqualQuitWhile(magicNumber);
							}
						
						if(getCurrentConstruct() == LogicConstructs.IFELSE)
							{
								if(currentType == Type.DOUBLE && isElsePresent())
									writeInstruction.ifLessOrEqualGotoElse(magicNumber);
								else if(currentType == Type.DOUBLE)
									writeInstruction.ifLessOrEqualQuitIfStat(magicNumber);
								else if(currentType == Type.INTEGER && isElsePresent())
									writeInstruction.ifIntLessOrEqualGotoElse(magicNumber);
								else writeInstruction.ifIntLessOrEqualQuitIfStat(magicNumber);
							}
						
						break;
						
				}
				
			}
		
		
		
		
		
// *************************************************************************************************************************************
// **************************************************** S U P P O R T I N G		 F U N C T I O N S *************************************
// *************************************************************************************************************************************

		
		
		public String getInstructions()
			{
				return writeInstruction.getInstructions();
			}
		
		private void setCompileError(boolean b)
			{
				compileError = b;
			}
		
		private void enterFunction()
			{
				functionSpace.push(new FunctionSpace());
			}
		
		private void exitFunction()
			{
				functionSpace.pop();
			}
		
		private void declareVar(String name, Type type)
			{
				if(type == Type.STRING)
					writeInstruction.pushValue("", type);
				else if(type == Type.BOOLEAN)
					writeInstruction.pushValue(0, type);
				else if(type == Type.INTEGER)
					writeInstruction.pushValue(0, type);
				else writeInstruction.pushValue(0.0, type);
					
				saveVar(name, type);
			}
		
		private void saveVar(String name, Type type)
			{
				saveInCurrentSpace(name, type);
				Integer index = getVarIndex(name);
				writeInstruction.saveVariable(type, index);
			}
		
		private void saveInCurrentSpace(String name, Type type)
			{
				functionSpace.peek().saveVariable(name, type);
			}
		
		private boolean isVarDeclared(String name)
			{
				return functionSpace.peek().isVarDeclared(name);
			}
		
		private Integer getVarIndex(String name)
			{
				return functionSpace.peek().getVarIndex(name);
			}
		
		private Type getVarType(String name)
			{
				return functionSpace.peek().getLocalType(name);
			}
		
		private void saveFunctionParamTypes(String name, List<Type> params)
			{
				functionParamTypes.put(name, params);
			}
		
		private void saveFunctionNoParams(String name, Integer number)
			{
				functionNoParams.put(name, number);
			}
		
		private Integer getNoParams(String name)
			{
				return functionNoParams.get(name);
			}
		
		public String getClassName()
			{
				return writeInstruction.getClassName();
			}
		
		private Type getNodeType(CommonTree node)
			{
				return utils.getNodeType(node, functionNames, functionSpace.peek());
			}
		
		public List<Type> getParamTypes(String name)
			{
				return functionParamTypes.get(name);
			}
		
		private String getPrintType(List<CommonTree> exprTree, CommonTree expr)
			{
				return utils.getStringFromType(execExpression(exprTree, expr));
			}
		
		private boolean isFunctionVoid(String name)
			{
				return ( functionNames.containsKey(name) &&  (functionNames.get(name) == null ) );
			}
		
		private boolean isFunctionDeclared(String name)
			{
				return functionNames.containsKey(name);
			}
		
		private void declareFunction(String name, Type type)
			{
				functionNames.put(name, type);
			}
		
		public Type getRetrunType(String name)
			{
				return functionNames.get(name);
			}
		
		private LogicConstructs getCurrentConstruct()
			{
				return functionSpace.peek().getConstructs().getCurrentConstruct();
			}
		
		private int getMagicNumber()
			{
				if(getCurrentConstruct() == LogicConstructs.WHILE)
					return functionSpace.peek().getConstructs().getWhileMagicNumber();
				else return functionSpace.peek().getConstructs().getIfElseMagicNumber();
			}
		
		private void exitWhileLoop()
			{
				functionSpace.peek().getConstructs().popWhile();
			}
		
		private void enterWhileLoop()
			{
				functionSpace.peek().getConstructs().pushWhile();
			}
		
		private void enterIfElse(Boolean isElsePresent)
			{
				functionSpace.peek().getConstructs().pushIfElse(isElsePresent);
			}
		
		private void exitIfElse()
			{
				functionSpace.peek().getConstructs().popIfElse();
			}
		
		private boolean isElsePresent()
			{
				return functionSpace.peek().getConstructs().isElsePresent();
			}
		
		public boolean getCompileStatus()
			{
				return compileError;
			}
		
		/**
		 * Enterance method for getLoohaheadType(List<CommonTree>, Type)
		 */
		private Type getLoohaheadType(List<CommonTree> nodes)
			{
				return utils.getLoohaheadType(nodes, Type.INVALID, functionNames, functionSpace.peek());
			}
		
	}
