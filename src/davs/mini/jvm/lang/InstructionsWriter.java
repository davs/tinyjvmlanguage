/**
 * Class contains all methods that are used to write bytecode instructions
 * 
 */

package davs.mini.jvm.lang;

public class InstructionsWriter
	{
		private StringBuilder instructions;
		private String nl = System.getProperty("line.separator");
		private String className;
		
		public InstructionsWriter(String fileName)
			{
				className = getTrimmedFilePath(fileName);
				instructions = new StringBuilder();
				write(".class public " + className);
				write(".super java/lang/Object");
				
				//constructor with no args
				write(".method <init>()V");
				write("aload_0");
				write("invokespecial java/lang/Object/<init>()V");
				write("return");
				methodEnd();
			}
		
		
		public void methodStart(String functionName, String params, String returnValue)
			{
				write(".method public static " + functionName + "(" + params + ")" + returnValue);
				limitStack(99); //At this stage not consered with optimization
			}
		
		public void methodEnd()
			{
				write(".end method");
			}
		
		public void startMain()
			{
				write(".method public static main([Ljava/lang/String;)V");
				limitStack(99); //At this stage not consered with optimization
			}
		
		public void returnFromMethod(String value)
			{
				if(value.equals("D"))
					write("dreturn");
				else if(value.equals("I") || value.equals("Z"))
					write("ireturn");
				else if(value.equals("Ljava/lang/String;"))
					write("areturn");
				else write("return");
			}
		
		public String getTrimmedFilePath(String fileName)
			{
				return fileName.substring(fileName.lastIndexOf("/") + 1).substring(fileName.lastIndexOf("\\") + 1);
			}
		
		public String getClassName()
			{
				return className;
			}
		
		public void stagePrint()
			{
				write("getstatic java/lang/System/out Ljava/io/PrintStream;");
			}

		public void print(String type)
			{
				write("invokevirtual java/io/PrintStream/println(" + type + ")V");
			}
		
		public void methodCall(String methodName, String args, String returnValue)
			{
				write("invokestatic " + className + "/" + methodName + "(" + args + ")" + returnValue);
			}
		
		public String getInstructions()
			{
				write("return");
				methodEnd();
				write(".end class");
				
				return instructions.toString();
			}
		
		private void write(String text)
			{
				instructions.append(text);
				instructions.append(nl); //Ensure that file always ends with an empty line
			}
		
		//varialbles related
		
		public void pushValue(Object value, Type type)
			{
				if(type == Type.INTEGER || type == Type.BOOLEAN)
					pushInteger(value);
				else if(type == Type.DOUBLE)
					pushDouble(value);
				else pushString(value);
			}
		
		private void pushInteger(Object value)
			{
				Integer i = (Integer) value;
				
				if(i == 0)
					write("iconst_0");
				else if(i == 1)
					write("iconst_1");
				else if(i == 2)
					write("iconst_2");
				else if(i == 3)
					write("iconst_3");
				else if(i == 4)
					write("iconst_4");
				else if(i == 5)
					write("iconst_5");
				else if(i == (-1))
					write("iconst_m1");
				else if(i <= 127 && i >= (-128))
					write("bipush "+ value);
				else write("sipush "+ value);
			
			}
		
		private void pushDouble(Object value)
			{
				Double d = (Double) value;
				
				if(d == 0)
					write("dconst_0");
				else if(d == 1)
					write("dconst_1");
				else write("ldc2_w " + value);
				
			}
		
		private void pushString(Object value)
			{
				write("ldc " + "\"" +  (String) value + "\"");
			}
		
		public void saveVariable(Type type, Integer index)
			{
				if(type == Type.DOUBLE)
					saveDouble(index);
				else if(type == Type.INTEGER || type==Type.BOOLEAN)
					saveInt(index);
				else saveString(index);
			}
		
		
		public void loadVariable(Type type, Integer index)
			{
				if(type == Type.DOUBLE)
					loadDouble(index);
				else if(type == Type.INTEGER || type==Type.BOOLEAN)
					loadInt(index);
				else loadString(index);
			}
		
		private void saveDouble(Integer index)
			{
				if(index == 0)
					write("dstore_0");
				else if(index == 1)
					write("dstore_1");
				else if(index == 2)
					write("dstore_2");
				else if(index == 3)
					write("dstore_3");
				else write("dstore " + index);
			}
		
		private void saveInt(Integer index)
			{
				if(index == 0)
					write("istore_0");
				else if(index == 1)
					write("istore_1");
				else if(index == 2)
					write("istore_2");
				else if(index == 3)
					write("istore_3");
				else write("istore " + index);
			}
		
		private void saveString(Integer index)
			{
				if(index == 0)
					write("astore_0");
				else if(index == 1)
					write("astore_1");
				else if(index == 2)
					write("astore_2");
				else if(index == 3)
					write("astore_3");
				else write("astore " + index);
			}
		
		private void loadDouble(Integer index)
			{
				if(index == 0)
					write("dload_0");
				else if(index == 1)
					write("dload_1");
				else if(index == 2)
					write("dload_2");
				else if(index == 3)
					write("dload_3");
				else write("dload " + index);
			}
		
		private void loadInt(Integer index)
			{
				if(index == 0)
					write("iload_0");
				else if(index == 1)
					write("iload_1");
				else if(index == 2)
					write("iload_2");
				else if(index == 3)
					write("iload_3");
				else write("iload "+index);
			}
		
		private void loadString(Integer index)
			{
				if(index == 0)
					write("aload_0");
				else if(index == 1)
					write("aload_1");
				else if(index == 2)
					write("aload_2");
				else if(index == 3)
					write("aload_3");
				else write("aload "+index);
			}
		
		public void limitStack(int x)
			{
				write(".limit stack " + x);
			}
		
		//arithmetics related
		
		public void subIntegers()
			{
				write("isub");
			}
		
		public void subDoubles()
			{
				write("dsub");
			}
		
		public void i2d()
			{
				write("i2d");
			}
		
		public void addIntegers()
			{
				write("iadd");
			}

		public void addDoubles()
			{
				write("dadd");
			}

		public void modIntegers()
			{
				write("irem");
			}
		
		public void modDoubles()
			{
				write("drem");
			}
		
		public void divIntegers()
			{
				write("idiv");
			}
		
		public void divDoubles()
			{
				write("ddiv");
			}
		
		public void multIntegers()
			{
				write("imul");
			}
		
		public void multDoubles()
			{
				write("dmul");
			}

		public void exponent()
			{
				write("invokestatic java/lang/Math/pow(DD)D");
			}
		
		public void negateInteger()
			{
				write("ineg");
			}
		
		public void negateDouble()
			{
				write("dneg");
			}
		
		private void compareDoubles()
			{
				write("dcmpl");
			}
		
		//while loop related...
		
		public void startWhile(int number)
			{
				write("start_while" + number + ":");
			}
		
		public void gotoStartWhile(int number)
			{
				write("goto start_while" + number);
			}
		
		public void endWhile(int number)
			{
				write("end_while" + number + ":");
			}
		
		public void ifEqualQuitWhile(int number)
			{
				compareDoubles();
				write("ifeq end_while" + number);
			}
		
		public void ifIntEqualQuitWhile(int number)
			{
				write("if_icmpeq end_while" + number);
			}

		public void ifNotEqualQuitWhile(int number)
			{
				compareDoubles();
				write("ifne end_while" + number);
			}
		
		public void ifIntNotEqualQuitWhile(int number)
			{
				write("if_icmpne end_while" + number);
			}
	
		public void ifGreaterQuitWhile(int number)
			{
				compareDoubles();
				write("ifgt end_while" + number);
			}
		
		public void ifIntGreaterQuitWhile(int number)
			{
				write("if_icmpgt end_while" + number);
			}
	
		public void ifGreaterOrEqualQuitWhile(int number)
			{
				compareDoubles();
				write("ifge end_while" + number);
			}
		
		public void ifIntGreaterOrEqualQuitWhile(int number)
			{
				write("if_icmpge end_while" + number);
			}
	
		public void ifLessOrEqualQuitWhile(int number)
			{
				compareDoubles();
				write("ifle end_while" + number);
			}
		
		public void ifIntLessOrEqualQuitWhile(int number)
			{
				write("if_icmple end_while" + number);
			}
	
		public void ifLessQuitWhile(int number)
			{
				compareDoubles();
				write("iflt end_while" + number);
			}
		
		public void ifIntLessQuitWhile(int number)
			{
				write("if_icmplt end_while" + number);
			}
		
		//if-else related...
		
		public void endIfElse(int number)
			{
				write("end_ifelse" + number + ":");
			}
		
		public void printElse(int number)
			{
				write("elsestat" + number + ":");
			}
		
		public void gotoIfEnd(int number)
			{
				write("goto end_ifelse" + number);
			}
		
		public void ifNotEqualQuitIfStat(int number)
			{
				compareDoubles();
				write("ifne end_ifelse" + number);
			}

		public void ifNotEqualGotoElse(int number)
			{
				compareDoubles();
				write("ifne elsestat" + number);
			}
		
		public void ifIntNotEqualQuitIfStat(int number)
			{
				write("if_icmpne end_ifelse" + number);
			}
		
		public void ifIntNotEqualGotoElse(int number)
			{
				write("if_icmpne elsestat" + number);
			}
	
		public void ifEqualQuitIfStat(int number)
			{
				compareDoubles();
				write("ifeq end_ifelse" + number);
			}
		
		public void ifEqualGotoElse(int number)
			{
				compareDoubles();
				write("ifeq elsestat" + number);
			}
		
		public void ifIntEqualQuitIfStat(int number)
			{
				write("if_icmpeq end_ifelse" + number);
			}
		
		public void ifIntEqualGotoElse(int number)
			{
				write("if_icmpeq elsestat" + number);
			}

		public void ifGreaterQuitIfStat(int number)
			{
				compareDoubles();
				write("ifgt end_ifelse" + number);
			}
		
		public void ifGreaterGotoElse(int number)
			{
				compareDoubles();
				write("ifgt elsestat" + number);
			}
		
		public void ifIntGreaterQuitIfStat(int number)
			{
				write("if_icmpgt end_ifelse" + number);
			}
		
		public void ifIntGreaterGotoElse(int number)
			{
				write("if_icmpgt elsestat" + number);
			}
	
		public void ifLessQuitIfStat(int number)
			{
				compareDoubles();
				write("iflt end_ifelse" + number);
			}
		
		public void ifLessGotoElse(int number)
			{
				compareDoubles();
				write("iflt elsestat" + number);
			}
		
		public void ifIntLessQuitIfStat(int number)
			{
				write("if_icmplt end_ifelse" + number);
			}
		
		public void ifIntLessGotoElse(int number)
			{
				write("if_icmplt elsestat" + number);
			}
	
		public void ifGreaterOrEqualQuitIfStat(int number)
			{
				compareDoubles();
				write("ifge end_ifelse" + number);
			}
		
		public void ifGreaterOrEqualGotoElse(int number)
			{
				compareDoubles();
				write("ifge elsestat" + number);
			}
		
		public void ifIntGreaterOrEqualQuitIfStat(int number)
			{
				write("if_icmpge end_ifelse" + number);
			}
		
		public void ifIntGreaterOrEqualGotoElse(int number)
			{
				write("if_icmpge elsestat" + number);
			}
	
		public void ifLessOrEqualQuitIfStat(int number)
			{
				compareDoubles();
				write("ifle end_ifelse" + number);
			}
		
		public void ifLessOrEqualGotoElse(int number)
			{
				compareDoubles();
				write("ifle elsestat" + number);
			}
		
		public void ifIntLessOrEqualQuitIfStat(int number)
			{
				write("if_icmple end_ifelse" + number);
			}
		
		public void ifIntLessOrEqualGotoElse(int number)
			{
				write("if_icmple elsestat" + number);
			}
	
	}
