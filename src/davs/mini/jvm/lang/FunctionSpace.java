/**
 * Contains all necessary data for each function's declaration.
 * Ideally loops and if-else statements would use line numbers in thier implementation,
 * however I decided to use labels with unique numbers approach.
 * 
 */

package davs.mini.jvm.lang;

import java.util.HashMap;

public class FunctionSpace
	{
		
		private HashMap<String, Integer> localsID; //Store index location of local
		private HashMap<String, Type> varTypes; //Store types for local variables
		private int indexCounter; //Index numbers for local variables
		
		private ConstructsSupport logicConstructs; //Holds all data to support while and if-else statements
		
		public FunctionSpace()
			{
				localsID = new HashMap<String, Integer>();
				varTypes = new HashMap<String, Type>();
				indexCounter = 0;
				logicConstructs = new ConstructsSupport();
			}
		
		public ConstructsSupport getConstructs()
			{
				return logicConstructs;
			}
		
		public boolean isVarDeclared(String name)
			{
				return localsID.containsKey(name.toLowerCase());
			}

		public Integer getVarIndex(String name)
			{
				return localsID.get(name.toLowerCase());
			}
		
		public Type getLocalType(String name)
			{
				return varTypes.get(name.toLowerCase());
			}
		
		public boolean saveVariable(String name, Type type)
			{
				
				if(indexCounter > 60000)
					{
						System.out.println("Max number of locals reached!!");
						return false;
					}
				
				varTypes.put(name.toLowerCase(), type);
				localsID.put(name.toLowerCase(), indexCounter);
				
				indexCounter++; //Next registry index for variables
				
				if(type==Type.DOUBLE)
					indexCounter++; //Doubles take two slots on the stack
				
				return true;
			}
		
	}
