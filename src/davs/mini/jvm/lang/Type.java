/**
 * Summary: Internal representation of types that are supported.
 * Mixed is either integer or double.(e.x. expression 2.0 + 2 is mixed, while 2 + 2 is not)
 *
 */

package davs.mini.jvm.lang;


public enum Type
	{
		STRING, DOUBLE, INTEGER, BOOLEAN, INVALID, MIXED;
	}
