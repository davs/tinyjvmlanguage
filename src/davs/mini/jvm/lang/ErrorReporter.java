/**
 * Class used to print error messages reported from interpreter.
 */

package davs.mini.jvm.lang;

public class ErrorReporter
	{
		
		private void error(String msg, int lineNumber)
			{
				System.out.println("Line " + lineNumber + " : " + msg);
			}
		
		private void error(String msg)
			{
				System.out.println(msg);
			}
		
		public void synatxErrors()
			{
				error("Syntax errors are present in a file!");
			}
		
		public void sourceNotFound()
			{
				error("Source file not found");
			}
		
		public void exitedWithError()
			{
				error("Program exited with error!");
			}
		
		public void noSuchVar(String msg, int lineNumber)
			{
				error("No such variable " + msg, lineNumber);
			}

		public void callSub(String msg, int lineNumber)
			{
				error("Sub must not return value : " + msg, lineNumber);
			}

		public void voidFunctNoReturn(String msg, int lineNumber)
			{
				error("Function " + msg + " is void function, can not return value", lineNumber);
			}
		
		public void wrongReturnType(String msg, int lineNumber)
			{
				error("Wrong return type : " + msg , lineNumber);
			}
		
		public void noSuchFunction(String msg, int lineNumber)
			{
				error("No such variable " + msg, lineNumber);
			}
		
		public void duplicateDeclaration(String msg)
			{
				error("Duplicate declaration " + msg);
			}
		
		public void invalidValue(String msg, int lineNumber)
			{
				error("Invalid value " + msg, lineNumber);
			}
		
		public void argsMissmatch(String msg, int lineNumber)
			{
				error("Arguments missmatch " + msg, lineNumber);
			}
		
		public void typeMissmatch(String msg, int lineNumber)
			{
				error("Type missmatch " + msg, lineNumber);
			}
		
		public void incompatibleDataType(int lineNumber)
			{
				error("Incompatible data type used", lineNumber);
			}
		
	}
