/**
 * Summary: Enable case insensitive input.
 * Code taken and adjusted from antlr site...
 *
 */

package davs.mini.jvm.lang;

import java.io.IOException;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;

public class NoCase extends ANTLRStringStream
	{
		public NoCase()
			{
			}

		public NoCase(String input) throws IOException
			{
				super(input);
			}

		NoCase(char[] data, int numberOfActualCharsInArray)
			{
				super(data, numberOfActualCharsInArray);
			}

		public int LA(int i)
			{
				if (i == 0)
					{
						return 0;
					}
				if (i < 0)
					{
						i++;
					}

				if ((p + i - 1) >= n)
					{
						return CharStream.EOF;
					}

				return Character.toUpperCase(data[p + i - 1]);
			}

	}
