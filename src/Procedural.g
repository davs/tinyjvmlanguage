grammar Procedural;

options{
  language = Java;
  output=AST;
  ASTLabelType = CommonTree;
}

//Custom tokens used in AST creation. 
tokens{
ASSIGN ;
BLOCK ;
CALLVOID ;
DUMMY ;
EXPRENTER ;
FUNCT ;
IFELSE ;
UNARY ;
VARDECLARE ;
VAR_OR_FUNCTCALL;
}

@members {

  Stack paraphrases = new Stack();
  
public String getErrorMessage(RecognitionException e, String[] tokenNames)
   {   
      
        String msg = super.getErrorMessage(e, tokenNames);

        if (e instanceof org.antlr.runtime.EarlyExitException)
          {
              msg = "Syntax error are present in a file, or file is empty!";
              return msg;
          }
        if (paraphrases.size() > 0)
          {
              String paraphrase = (String) paraphrases.peek();
              msg = msg + " " + paraphrase;
          }

        return msg;
   }
  

}

@header {  package davs.mini.jvm.lang; }

@lexer::header {  package davs.mini.jvm.lang;  }



//*************************************************************************************************************************************
//******************************************************  P A R S E R *****************************************************************
//*************************************************************************************************************************************


program:
  functionDef* block EOF -> ^(BLOCK functionDef* block)
;


block:
  slist -> ^(BLOCK slist)  
;


functionDef:  
(
    FUNCTION r=types? id=ID   (LPAREN  (par+=types p+=ID (COMMA par+=types p+=ID)* )? RPAREN)? 
    s=slist
    RETURN e=expression?    
    END FUNCTION 
)    -> ^(FUNCT $r? DUMMY  $id $s  $e? DUMMY ($par $p)*)
;


slist:
   statement+ -> ^(BLOCK statement+)
;


types:
(STR | INT | DBL | BLN)
;


//*************************************************************************************************************************************
//****************************************************** S T A T E M E N T S **********************************************************
//*************************************************************************************************************************************


statement
@init  { paraphrases.push("in statement"); }
@after { paraphrases.pop(); }
:
print -> print
|
assignment -> assignment
|
declareVars -> declareVars
|
callSub -> callSub
|
ifStatement -> ifStatement
|
whileLoop -> whileLoop
;


print
@init  { paraphrases.push("in print statement"); }
@after { paraphrases.pop(); }
:
  PRINT expression -> ^(PRINT expression)
;


assignment
@init  { paraphrases.push("in assignment"); }
@after { paraphrases.pop(); }
:
id=ID EQ expression -> ^(ASSIGN $id expression)
;


declareVars
@init  { paraphrases.push("in variable declaration"); }
@after { paraphrases.pop(); }
:
DECLARE varName=ID AS t=types -> ^(VARDECLARE $varName $t) 
;


callSub
@init  { paraphrases.push("in variable declaration"); }
@after { paraphrases.pop(); }
:
CALL ID (LPAREN e+=expression? (COMMA e+=expression)*  RPAREN)? -> ^(CALLVOID ID LPAREN?  $e*)
;


ifStatement
@init  { paraphrases.push("in if-else statement"); }
@after { paraphrases.pop(); }
:
  IF mainIf=expression  THEN  (mainSlist=slist)
  (ELSE elseSlist=slist)? 
  END IF
  -> ^(IFELSE $mainIf $mainSlist $elseSlist?)
;


whileLoop
@init  { paraphrases.push("in while loop "); }
@after { paraphrases.pop(); }
:  
  WHILE expression
  slist
  END WHILE
  -> ^(WHILE expression slist)
;


//*************************************************************************************************************************************
//***************************************************** P A R S E R - E X P R E S S I O N S *******************************************
//*************************************************************************************************************************************


//Eventually expressions evaluate to one shown bellow! Recursive chain ends here with atom, and starts with expression rule.
atom:  
  BOOLEAN
  |
  STRING  
  |
  INTEGER 
  |
  DOUBLE 
  |
  ID (LPAREN e+=expression? (COMMA e+=expression)*  RPAREN)? -> ^(VAR_OR_FUNCTCALL ID LPAREN?  $e*)
  |
  LPAREN expression RPAREN -> ^(EXPRENTER expression)
;

exponentiation:
  atom (EXP^ atom)*
;

unary:
  (m=MINUS)? exponentiation //e.x. -3,-2
  ->{(m != null)}? ^(UNARY exponentiation) //if there is no minus, just return number
  -> exponentiation
;

multiply:
  unary ( (MULT | DIV | MOD)^ unary)*
;

addition:
  multiply ( (PLUS | MINUS)^ multiply)*
;

expression
@init  { paraphrases.push("in expression"); }
@after { paraphrases.pop(); }
:
  addition ( (EQ | NOTEQ | LT | LTEQ | GTEQ | GT)^  addition)*
;

// Boolean operators (and/or) are currently left out of language, since implementation would take fair chunk of time


//*************************************************************************************************************************************
//************************* R E S E R V E D     K E Y W O R D S      &      S P E C I A L   C H A R A C T E R S ***********************
//*************************************************************************************************************************************


AS        :    'AS' ;
CALL      :    'CALL' ;
DECLARE   :    'DECLARE' ;
ELSE      :    'ELSE' ;
END       :    'END' ;
FUNCTION  :    'FUNCTION' ;
IF        :    'IF' ;
MOD       :    'MOD' ;
PRINT     :    'PRINT' ;
REMARK    :    '#' ;
RETURN    :    'RETURN' ;
THEN      :    'THEN' ;
WHILE     :    'WHILE' ;
LTEQ      :    '<=' ;
LT        :    '<' ;
GTEQ      :    '>=' ;
GT        :    '>' ;
EQ        :    '=' ;
NOTEQ     :    '<>' ;
MINUS     :    '-' ;
PLUS      :    '+' ;
DIV       :    '/' ;
MULT      :    '*' ;
EXP       :    '^' ;
COMMA     :    ',' ;
LPAREN    :    '(';
RPAREN    :    ')' ;

//Actual words typed, not data types
INT       :    'INTEGER';
DBL       :    'DOUBLE';
STR       :    'STRING';
BLN       :    'BOOLEAN';


//*************************************************************************************************************************************
//********************************************************* L E X E R *****************************************************************
//*************************************************************************************************************************************


fragment NL: ( '\r'? '\n' | '\r' ) ;
fragment LETTER : ('a'..'z' | 'A'..'Z') ;
fragment DIGIT : '0'..'9' ;
COMMENT: REMARK (options{greedy=false;}: .)* NL {$channel=HIDDEN;} ;
BOOLEAN: 'TRUE' | 'FALSE' ;
INTEGER: DIGIT+ ;
DOUBLE: INTEGER '.' INTEGER* | '.' INTEGER+ ;
ID: LETTER (LETTER | DIGIT)* ;
STRING: '\"' .* '\"' ;
WS: (' ' | '\t' | '\n' | '\r' | '\f')+ {$channel = HIDDEN;} ;
