#! /bin/bash
cd ExponentRule
java -jar ../../lib/Oolong.jar exponent
java exponent > outputedResults

if diff results outputedResults > /dev/zero ; then
	rm exponent.class
	echo "" > outputedResults
	echo "Exponent test passed!"
else
	echo "Exponent rule tests failed to produce correct results!"	
fi

cd ..
