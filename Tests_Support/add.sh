#! /bin/bash
cd AdditionRule
java -jar ../../lib/Oolong.jar add
java add > outputedResults

if diff results outputedResults > /dev/zero ; then
	rm add.class
	echo "" > outputedResults
	echo "Addition test passed!"
else
	echo "Addition rule tests failed to produce correct results!"	
fi

cd ..
