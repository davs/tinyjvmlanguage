#! /bin/bash

echo "Executing tests...................................................................."

sh exponent.sh
sh unary.sh
sh mult.sh
sh add.sh
sh var.sh
sh voidFuncts.sh
sh nonVoidFunct.sh
sh whileLoop.sh
sh ifelse.sh

echo "Done!!!!!"
