#! /bin/bash
cd NonVoidFunctions

java -jar ../../lib/Oolong.jar stringfunct
java stringfunct > stringOutput

if diff stringResults stringOutput > /dev/zero ; then
	rm stringfunct.class
	echo "" > stringOutput
	echo "String function test passed!"
else
	echo "String function rule failed to produce correct results!"	
fi

java -jar ../../lib/Oolong.jar numbersfunct
java numbersfunct > numbersOutput

if diff numbersOutput numbersResult > /dev/zero ; then
	rm numbersfunct.class
	echo "" > numbersOutput
	echo "Numbers function test passed!"
else
	echo "Numbers function rule failed to produce correct results!"	
fi

cd ..