#! /bin/bash
cd VarTests
java -jar ../../lib/Oolong.jar var
java var > outputedResults

if diff results outputedResults > /dev/zero ; then
	rm var.class
	echo "" > outputedResults
	echo "Variables test passed!"
else
	echo "Variables tests failed to produce correct results!"	
fi

cd ..
