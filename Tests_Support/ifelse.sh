#! /bin/bash
cd IfElse
java -jar ../../lib/Oolong.jar ifelse
java ifelse > ifelseOutput

if diff ifelseOutput ifelseResults > /dev/zero ; then
	rm ifelse.class
	echo "" > ifelseOutput
	echo "If-Else test passed!"
else
	echo "If-Else rule tests failed to produce correct results!"	
fi

cd ..
