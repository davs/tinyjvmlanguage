#! /bin/bash
cd VoidFunctions
java -jar ../../lib/Oolong.jar voidfunct
java voidfunct > voidOutput

if diff voidResults voidOutput > /dev/zero ; then
	rm voidfunct.class
	echo "" > voidOutput
	echo "Void funct test passed!"
else
	echo "Void funct test failed to produce correct results!"	
fi

java -jar ../../lib/Oolong.jar voidfunctargs
java voidfunctargs > voidargsOutput

if diff voidargsResults voidargsOutput > /dev/zero ; then
	rm voidfunctargs.class
	echo "" > voidargsOutput
	echo "Void funct with args test passed!"
else
	echo "Void funct with args test failed to produce correct results!"	
fi

cd ..
