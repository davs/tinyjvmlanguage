#! /bin/bash
cd UnaryRule
java -jar ../../lib/Oolong.jar unary
java unary > outputedResults

if diff results outputedResults > /dev/zero ; then
	rm unary.class
	echo "" > outputedResults
	echo "Unary test passed!"
else
	echo "Unary rule tests failed to produce correct results!"	
fi

cd ..
