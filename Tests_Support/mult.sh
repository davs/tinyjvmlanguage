#! /bin/bash
cd MultiplyRule
java -jar ../../lib/Oolong.jar mult
java mult > outputedResults

if diff results outputedResults > /dev/zero ; then
	rm mult.class
	echo "" > outputedResults
	echo "Multiply test passed!"
else
	echo "Multiply rule failed to produce correct results!"	
fi

cd ..
