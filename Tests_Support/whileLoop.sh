#! /bin/bash
cd WhileLoop
java -jar ../../lib/Oolong.jar whileLoop
java whileLoop > whileOutput

if diff whileResults whileOutput > /dev/zero ; then
	rm whileLoop.class
	echo "" > whileOutput
	echo "While..Loop test passed!"
else
	echo "While...Loop tests failed to produce correct results!"	
fi

cd ..
