package davs.tests.minilang;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import davs.mini.jvm.lang.MainProgram;
import davs.test.addition.AdditionSuite;
import davs.test.exponent.ExponentSuite;
import davs.test.functions.FunctsSuite;
import davs.test.ifelse.IfElseSuite;
import davs.test.multiply.MultiplySuite;
import davs.test.unary.UnarySuite;
import davs.test.vars.VarSuite;
import davs.test.whileloop.WhileLoopSuite;

@RunWith(Suite.class)
@SuiteClasses(
	{
		ExponentSuite.class ,
		UnarySuite.class ,
		MultiplySuite.class ,
		AdditionSuite.class ,
		VarSuite.class ,
		FunctsSuite.class ,
		WhileLoopSuite.class ,
		IfElseSuite.class
	
	})

public class MasterSuite
	{
		
		public static boolean compareTextFiles(String pathToSource, String pathToExpectedOutput) throws IOException, RecognitionException
			{
				String outputGeneratedByTests = MainProgram.getInstructions(pathToSource);
				String expectedOutput = MainProgram.readFileToString(pathToExpectedOutput);
				
				return expectedOutput.equals(outputGeneratedByTests);
			}
		
		
	}
