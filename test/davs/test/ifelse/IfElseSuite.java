package davs.test.ifelse;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
	{ IfElseTest.class })
public class IfElseSuite
	{

	}
