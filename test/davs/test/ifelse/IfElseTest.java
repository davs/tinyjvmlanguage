package davs.test.ifelse;

import static org.junit.Assert.*;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class IfElseTest
	{

		@Test
		public void test() throws IOException, RecognitionException
			{
				String path = "./Tests_Support/IfElse/ifelse.mini";
				String expect = "./Tests_Support/IfElse/ifelse";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("If-Else instructions test failed!" , isContentSame);
			}

	}
