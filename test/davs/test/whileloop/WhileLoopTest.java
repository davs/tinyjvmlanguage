package davs.test.whileloop;

import static org.junit.Assert.*;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class WhileLoopTest
	{

		@Test
		public void test() throws IOException, RecognitionException
			{
				String path = "./Tests_Support/WhileLoop/whileLoop.mini";
				String expect = "./Tests_Support/WhileLoop/whileLoop";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("While loop instructions test failed!" , isContentSame);
			}

	}
