package davs.test.whileloop;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
	{ WhileLoopTest.class })
public class WhileLoopSuite
	{

	}
