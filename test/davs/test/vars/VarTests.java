package davs.test.vars;

import static org.junit.Assert.*;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class VarTests
	{

		@Test
		public void test() throws IOException, RecognitionException
			{
				String path = "./Tests_Support/VarTests/var.mini";
				String expect = "./Tests_Support/VarTests/var";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Variable instructions test failed!" , isContentSame);
			}

	}
