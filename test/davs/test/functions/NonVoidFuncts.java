package davs.test.functions;

import static org.junit.Assert.*;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class NonVoidFuncts
	{

		@Test
		public void test() throws IOException, RecognitionException
			{
				
				String path = "./Tests_Support/NonVoidFunctions/stringfunct.mini";
				String expect = "./Tests_Support/NonVoidFunctions/stringfunct";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("String functs test failed!" , isContentSame);

				path = "./Tests_Support/NonVoidFunctions/numbersfunct.mini";
				expect = "./Tests_Support/NonVoidFunctions/numbersfunct";
				isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Number functs test failed!" , isContentSame);
				
			}

	}
