package davs.test.functions;

import static org.junit.Assert.*;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class VoidFuncts
	{

		@Test
		public void test() throws IOException, RecognitionException
			{
				String path = "./Tests_Support/VoidFunctions/voidfunct.mini";
				String expect = "./Tests_Support/VoidFunctions/voidfunct";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Void functs test failed!" , isContentSame);

				path = "./Tests_Support/VoidFunctions/voidfunctargs.mini";
				expect = "./Tests_Support/VoidFunctions/voidfunctargs";
				isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Void funct with args test failed!" , isContentSame);
			}

	}
