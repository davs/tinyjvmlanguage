package davs.test.functions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
	{ NonVoidFuncts.class, VoidFuncts.class })
public class FunctsSuite
	{

	}
