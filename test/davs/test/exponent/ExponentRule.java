package davs.test.exponent;

import static org.junit.Assert.*;

import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class ExponentRule
	{
		
		@Test
		public void test() throws Exception
			{
				String path = "./Tests_Support/ExponentRule/exponent.mini";
				String expect = "./Tests_Support/ExponentRule/exponent";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Exponent instructions test failed!" , isContentSame);
			}

	}
