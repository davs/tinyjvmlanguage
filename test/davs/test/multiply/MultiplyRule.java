package davs.test.multiply;

import static org.junit.Assert.*;

import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class MultiplyRule
	{
		
		@Test
		public void test() throws Exception
			{
				String path = "./Tests_Support/MultiplyRule/mult.mini";
				String expect = "./Tests_Support/MultiplyRule/mult";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Multiply instructions test failed!" , isContentSame);
			}
		
	}
