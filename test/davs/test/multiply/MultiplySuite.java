package davs.test.multiply;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
	{ MultiplyRule.class })
public class MultiplySuite
	{

	}
