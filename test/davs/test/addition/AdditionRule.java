package davs.test.addition;

import static org.junit.Assert.*;

import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class AdditionRule
	{

		@Test
		public void test() throws Exception
			{
				String path = "./Tests_Support/AdditionRule/add.mini";
				String expect = "./Tests_Support/AdditionRule/add";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Addition instructions test failed!" , isContentSame);
			}

	}
