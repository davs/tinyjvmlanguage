package davs.test.unary;

import static org.junit.Assert.*;

import org.junit.Test;

import davs.tests.minilang.MasterSuite;

public class UnaryRule
	{
		
		@Test
		public void test() throws Exception
			{
				String path = "./Tests_Support/UnaryRule/unary.mini";
				String expect = "./Tests_Support/UnaryRule/unary";
				boolean isContentSame = MasterSuite.compareTextFiles(path, expect);
				assertTrue("Unary instructions test failed!" , isContentSame);
			}
		
	}
