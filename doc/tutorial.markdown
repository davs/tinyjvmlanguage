# Compiling tinyjvmlang files

In bin folder you may find tinyjvmlang.jar, which is used to compile tinyjvmlang source files.
Tinyjvmlang source files must end with ".mini" extension. So, let's assume you have copied tinyjvmlang.jar
in your current folder and created file named test.mini which contains tinyjvmlang source code.
To compile test.mini you would run following command:

    java - jar tinyjvmlang.jar test.mini

If you would like to see which instructions were generated you may append -i to previous command like this:

    java - jar tinyjvmlang.jar test.mini -i

Since tinyjvmlang is JVM language take a look at your current folder, if there were no errors you should see
new file created, named test.class. If you are not familiar with class files, this is same file format that
Java compiler produces, and it can be loaded by any Java Virtual Machine. So, now you can type following command
to execute newly created class file, just like you would execure any Java program:

    java test


# Variables

Tinyjvmlang supports integers, doubles, booleans and strings. Before using variable you have to declare it.
Following snippet declares four variables, assigns appropriate values for each variable, and finally prints
value of each variable to the screen:

    DECLARE A AS INTEGER
    DECLARE B AS DOUBLE
    DECLARE C AS BOOLEAN
    DECLARE D AS STRING

    A = 10
    B = 5.0
    C = TRUE
    D = "This is a string"

    PRINT A
    PRINT B
    PRINT C
    PRINT D

After compiling and executing .class file you would see following output (note that booleans are actually numbers):

    10
    5.0
    1
    This is a string


# Arithmetic operations

Following code snippet shows all supported arithmetic operators, note that every line starting with # is a comment and is ignored:

    # First declare two variables
    DECLARE A AS DOUBLE
    DECLARE B AS DOUBLE

    # Assign some values to variables
    A = 10.0
    B = 5.0

    # Add and subtract two numbers
    PRINT A + B # prints 15.0
    PRINT A - B # prints 5.0

    # Multiply, divide and modulo operations
    PRINT A * B # prints 50.0
    PRINT A / B # prints 2.0
    PRINT 12 MOD 5 # prints 2.0

    # Unary minus and exponent
    PRINT 2^2 # prints 4
    PRINT -A # prints -10.0

    # Oh, and you can do assignment like this:
    A = B
    PRINT A # prints 5.0, not 10.0


# If-Else statements and relational operators

Following snippet shows use of relational operators, currently logical and/or is not implemented so it is
not possible to group relational operators.

    IF 10 > 100 THEN # is 10 greater than 100  
    PRINT 10 
    ELSE PRINT 100 # prints 100, since 10 is not greater than 100
    END IF

    IF 10 < 100 THEN # is 10 less than 100
    PRINT 10 # prints 10
    END IF # note that else clause is optional

    IF 10 = 100 THEN # is 10 equal 100
    PRINT 10 # nothing is printed since 10 is not equal 100
    END IF

    IF 10 <> 100 THEN # is 10 different from 100
    PRINT 10 # prints 10
    END IF

    # You can also nest if statements
    IF 10 <= 100 THEN # is 10 less or equal than 100
    PRINT "passed..." # this will be printed    
    IF 10 >= 100 THEN # is 10 greater or equal than 100
    PRINT 10 # this will not be printed
    END IF
    END IF


# While loop

In while loop you use use same relational operators used in section before.
Following code shows how to loop from 1 to 5:

    DECLARE INCREMENT AS INTEGER
    INCREMENT = 1

    WHILE(INCREMENT < 6)
    PRINT INCREMENT # prints 1..5 as it loops
    INCREMENT = INCREMENT + 1
    END WHILE

Just like if statements, you can also nest while loop one inside others.


# Functions

Functions in tinyjvmlang must be declared at the top of a file before they are used. Function _must_ have at least one statement,
and must end with a return statement. Here is classic example which calculates n-th Fibonacci's number.

    #To declare function start with function keyword, type that function returns
    # name of functions and optional parameters listed in parentheses separated by commas
    FUNCTION INTEGER FIB(INTEGER PARAM1)
    
    # There must be at least one statement
  
    DECLARE TMP AS INTEGER
    TMP = PARAM1

    IF PARAM1 <= 2 THEN
    TMP = 1
    ELSE
    TMP = FIB(TMP - 1) + FIB(TMP - 2)
    END IF

    RETURN TMP # return always must be oresent at the end
    END FUNCTION


    PRINT FIB(10) # prints 55


If you leave out return value, and return type function returns no value, and must be called like this:

    FUNCTION VOIDFUNCT() #Just function keyword and function's name, no return type
    PRINT "Something..."
    RETURN # Nothing after return keyword
    END FUNCTION
   
    # SOMETHING = VOIDFUNCT() --> this produces error, since function does not return any value
    CALL VOIDFUNCT() # Instead explicitly call using call keyword







