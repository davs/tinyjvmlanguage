# Intro

As you might expect tinyjvmlang is, very small toy language written for Java Virtual Machine. Antlr is used for front-end (AST creation).
Assembly of choice is jasmin based langage named oolong. I preferred it over jasmin because I found documentation slightly easier to use. Java is
used to interpret AST and output appropriate oolong instructions. Being "toy language" , tinyjvmlang produces quite _unoptimized_ bytecode instructions.
Best place to get started is to go to doc folder and read tutorial.markdown to learn how to use tinyjvmlang and to see what features are currently supported.

Finally, who might be interested to take a look at source code ? Well, if you are looking to create a quick JVM language (or for some other platform) , and you don't know where to start,
or perhaps you just finished some Antlr and/or Java tutorials and are looking for small/medium sized project to get idea on how to use knowledge gained from tutorials, you might benefit
from taking a peek at the source code. Have in mind, more serious project would probably use templates to output bytecode instructions and would produce more optimized instructions. 


# Project folder structure

* _antlr-generated_ Contains code generated from the grammar file.
* _bin_ Precompiled jar file can be found here.
* _doc_ Tutorial files which show how to write and run tinyjvmlang programs.
* _lib_ External libraries used in tinyjvmlang creation.
* _src_ Contains full tinyjvmlang source code.
* _test and Test_Support folders_ Files used to test tinyjvmlang.


# Licence

[freebsd licence](http://www.freebsd.org/copyright/freebsd-license.html)
