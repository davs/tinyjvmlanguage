// $ANTLR 3.3 Nov 30, 2010 12:50:56 /home/davors/git/miniJVM/MiniJVM/src/Procedural.g 2012-10-07 12:13:08
  package davs.mini.jvm.lang; 

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class ProceduralParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ASSIGN", "BLOCK", "CALLVOID", "DUMMY", "EXPRENTER", "FUNCT", "IFELSE", "UNARY", "VARDECLARE", "VAR_OR_FUNCTCALL", "FUNCTION", "ID", "LPAREN", "COMMA", "RPAREN", "RETURN", "END", "STR", "INT", "DBL", "BLN", "PRINT", "EQ", "DECLARE", "AS", "CALL", "IF", "THEN", "ELSE", "WHILE", "BOOLEAN", "STRING", "INTEGER", "DOUBLE", "EXP", "MINUS", "MULT", "DIV", "MOD", "PLUS", "NOTEQ", "LT", "LTEQ", "GTEQ", "GT", "REMARK", "NL", "LETTER", "DIGIT", "COMMENT", "WS"
    };
    public static final int EOF=-1;
    public static final int ASSIGN=4;
    public static final int BLOCK=5;
    public static final int CALLVOID=6;
    public static final int DUMMY=7;
    public static final int EXPRENTER=8;
    public static final int FUNCT=9;
    public static final int IFELSE=10;
    public static final int UNARY=11;
    public static final int VARDECLARE=12;
    public static final int VAR_OR_FUNCTCALL=13;
    public static final int FUNCTION=14;
    public static final int ID=15;
    public static final int LPAREN=16;
    public static final int COMMA=17;
    public static final int RPAREN=18;
    public static final int RETURN=19;
    public static final int END=20;
    public static final int STR=21;
    public static final int INT=22;
    public static final int DBL=23;
    public static final int BLN=24;
    public static final int PRINT=25;
    public static final int EQ=26;
    public static final int DECLARE=27;
    public static final int AS=28;
    public static final int CALL=29;
    public static final int IF=30;
    public static final int THEN=31;
    public static final int ELSE=32;
    public static final int WHILE=33;
    public static final int BOOLEAN=34;
    public static final int STRING=35;
    public static final int INTEGER=36;
    public static final int DOUBLE=37;
    public static final int EXP=38;
    public static final int MINUS=39;
    public static final int MULT=40;
    public static final int DIV=41;
    public static final int MOD=42;
    public static final int PLUS=43;
    public static final int NOTEQ=44;
    public static final int LT=45;
    public static final int LTEQ=46;
    public static final int GTEQ=47;
    public static final int GT=48;
    public static final int REMARK=49;
    public static final int NL=50;
    public static final int LETTER=51;
    public static final int DIGIT=52;
    public static final int COMMENT=53;
    public static final int WS=54;

    // delegates
    // delegators


        public ProceduralParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public ProceduralParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return ProceduralParser.tokenNames; }
    public String getGrammarFileName() { return "/home/davors/git/miniJVM/MiniJVM/src/Procedural.g"; }



      Stack paraphrases = new Stack();
      
    public String getErrorMessage(RecognitionException e, String[] tokenNames)
       {   
          
            String msg = super.getErrorMessage(e, tokenNames);

            if (e instanceof org.antlr.runtime.EarlyExitException)
              {
                  msg = "Syntax error are present in a file, or file is empty!";
                  return msg;
              }
            if (paraphrases.size() > 0)
              {
                  String paraphrase = (String) paraphrases.peek();
                  msg = msg + " " + paraphrase;
              }

            return msg;
       }
      



    public static class program_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "program"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:60:1: program : ( functionDef )* block EOF -> ^( BLOCK ( functionDef )* block ) ;
    public final ProceduralParser.program_return program() throws RecognitionException {
        ProceduralParser.program_return retval = new ProceduralParser.program_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token EOF3=null;
        ProceduralParser.functionDef_return functionDef1 = null;

        ProceduralParser.block_return block2 = null;


        CommonTree EOF3_tree=null;
        RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
        RewriteRuleSubtreeStream stream_functionDef=new RewriteRuleSubtreeStream(adaptor,"rule functionDef");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:60:8: ( ( functionDef )* block EOF -> ^( BLOCK ( functionDef )* block ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:61:3: ( functionDef )* block EOF
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:61:3: ( functionDef )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==FUNCTION) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:61:3: functionDef
            	    {
            	    pushFollow(FOLLOW_functionDef_in_program114);
            	    functionDef1=functionDef();

            	    state._fsp--;

            	    stream_functionDef.add(functionDef1.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            pushFollow(FOLLOW_block_in_program117);
            block2=block();

            state._fsp--;

            stream_block.add(block2.getTree());
            EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_program119);  
            stream_EOF.add(EOF3);



            // AST REWRITE
            // elements: functionDef, block
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 61:26: -> ^( BLOCK ( functionDef )* block )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:61:29: ^( BLOCK ( functionDef )* block )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, "BLOCK"), root_1);

                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:61:37: ( functionDef )*
                while ( stream_functionDef.hasNext() ) {
                    adaptor.addChild(root_1, stream_functionDef.nextTree());

                }
                stream_functionDef.reset();
                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "program"

    public static class block_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "block"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:65:1: block : slist -> ^( BLOCK slist ) ;
    public final ProceduralParser.block_return block() throws RecognitionException {
        ProceduralParser.block_return retval = new ProceduralParser.block_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        ProceduralParser.slist_return slist4 = null;


        RewriteRuleSubtreeStream stream_slist=new RewriteRuleSubtreeStream(adaptor,"rule slist");
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:65:6: ( slist -> ^( BLOCK slist ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:66:3: slist
            {
            pushFollow(FOLLOW_slist_in_block141);
            slist4=slist();

            state._fsp--;

            stream_slist.add(slist4.getTree());


            // AST REWRITE
            // elements: slist
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 66:9: -> ^( BLOCK slist )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:66:12: ^( BLOCK slist )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, "BLOCK"), root_1);

                adaptor.addChild(root_1, stream_slist.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "block"

    public static class functionDef_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "functionDef"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:70:1: functionDef : ( FUNCTION (r= types )? id= ID ( LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN )? s= slist RETURN (e= expression )? END FUNCTION ) -> ^( FUNCT ( $r)? DUMMY $id $s ( $e)? DUMMY ( $par $p)* ) ;
    public final ProceduralParser.functionDef_return functionDef() throws RecognitionException {
        ProceduralParser.functionDef_return retval = new ProceduralParser.functionDef_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token id=null;
        Token FUNCTION5=null;
        Token LPAREN6=null;
        Token COMMA7=null;
        Token RPAREN8=null;
        Token RETURN9=null;
        Token END10=null;
        Token FUNCTION11=null;
        Token p=null;
        List list_p=null;
        List list_par=null;
        ProceduralParser.types_return r = null;

        ProceduralParser.slist_return s = null;

        ProceduralParser.expression_return e = null;

        RuleReturnScope par = null;
        CommonTree id_tree=null;
        CommonTree FUNCTION5_tree=null;
        CommonTree LPAREN6_tree=null;
        CommonTree COMMA7_tree=null;
        CommonTree RPAREN8_tree=null;
        CommonTree RETURN9_tree=null;
        CommonTree END10_tree=null;
        CommonTree FUNCTION11_tree=null;
        CommonTree p_tree=null;
        RewriteRuleTokenStream stream_FUNCTION=new RewriteRuleTokenStream(adaptor,"token FUNCTION");
        RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_END=new RewriteRuleTokenStream(adaptor,"token END");
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
        RewriteRuleTokenStream stream_RETURN=new RewriteRuleTokenStream(adaptor,"token RETURN");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_types=new RewriteRuleSubtreeStream(adaptor,"rule types");
        RewriteRuleSubtreeStream stream_slist=new RewriteRuleSubtreeStream(adaptor,"rule slist");
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:70:12: ( ( FUNCTION (r= types )? id= ID ( LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN )? s= slist RETURN (e= expression )? END FUNCTION ) -> ^( FUNCT ( $r)? DUMMY $id $s ( $e)? DUMMY ( $par $p)* ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:71:1: ( FUNCTION (r= types )? id= ID ( LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN )? s= slist RETURN (e= expression )? END FUNCTION )
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:71:1: ( FUNCTION (r= types )? id= ID ( LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN )? s= slist RETURN (e= expression )? END FUNCTION )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:5: FUNCTION (r= types )? id= ID ( LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN )? s= slist RETURN (e= expression )? END FUNCTION
            {
            FUNCTION5=(Token)match(input,FUNCTION,FOLLOW_FUNCTION_in_functionDef168);  
            stream_FUNCTION.add(FUNCTION5);

            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:15: (r= types )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>=STR && LA2_0<=BLN)) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:15: r= types
                    {
                    pushFollow(FOLLOW_types_in_functionDef172);
                    r=types();

                    state._fsp--;

                    stream_types.add(r.getTree());

                    }
                    break;

            }

            id=(Token)match(input,ID,FOLLOW_ID_in_functionDef177);  
            stream_ID.add(id);

            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:31: ( LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==LPAREN) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:32: LPAREN (par+= types p+= ID ( COMMA par+= types p+= ID )* )? RPAREN
                    {
                    LPAREN6=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_functionDef182);  
                    stream_LPAREN.add(LPAREN6);

                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:40: (par+= types p+= ID ( COMMA par+= types p+= ID )* )?
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( ((LA4_0>=STR && LA4_0<=BLN)) ) {
                        alt4=1;
                    }
                    switch (alt4) {
                        case 1 :
                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:41: par+= types p+= ID ( COMMA par+= types p+= ID )*
                            {
                            pushFollow(FOLLOW_types_in_functionDef188);
                            par=types();

                            state._fsp--;

                            stream_types.add(par.getTree());
                            if (list_par==null) list_par=new ArrayList();
                            list_par.add(par.getTree());

                            p=(Token)match(input,ID,FOLLOW_ID_in_functionDef192);  
                            stream_ID.add(p);

                            if (list_p==null) list_p=new ArrayList();
                            list_p.add(p);

                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:58: ( COMMA par+= types p+= ID )*
                            loop3:
                            do {
                                int alt3=2;
                                int LA3_0 = input.LA(1);

                                if ( (LA3_0==COMMA) ) {
                                    alt3=1;
                                }


                                switch (alt3) {
                            	case 1 :
                            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:72:59: COMMA par+= types p+= ID
                            	    {
                            	    COMMA7=(Token)match(input,COMMA,FOLLOW_COMMA_in_functionDef195);  
                            	    stream_COMMA.add(COMMA7);

                            	    pushFollow(FOLLOW_types_in_functionDef199);
                            	    par=types();

                            	    state._fsp--;

                            	    stream_types.add(par.getTree());
                            	    if (list_par==null) list_par=new ArrayList();
                            	    list_par.add(par.getTree());

                            	    p=(Token)match(input,ID,FOLLOW_ID_in_functionDef203);  
                            	    stream_ID.add(p);

                            	    if (list_p==null) list_p=new ArrayList();
                            	    list_p.add(p);


                            	    }
                            	    break;

                            	default :
                            	    break loop3;
                                }
                            } while (true);


                            }
                            break;

                    }

                    RPAREN8=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_functionDef210);  
                    stream_RPAREN.add(RPAREN8);


                    }
                    break;

            }

            pushFollow(FOLLOW_slist_in_functionDef221);
            s=slist();

            state._fsp--;

            stream_slist.add(s.getTree());
            RETURN9=(Token)match(input,RETURN,FOLLOW_RETURN_in_functionDef227);  
            stream_RETURN.add(RETURN9);

            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:74:13: (e= expression )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=ID && LA6_0<=LPAREN)||(LA6_0>=BOOLEAN && LA6_0<=DOUBLE)||LA6_0==MINUS) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:74:13: e= expression
                    {
                    pushFollow(FOLLOW_expression_in_functionDef231);
                    e=expression();

                    state._fsp--;

                    stream_expression.add(e.getTree());

                    }
                    break;

            }

            END10=(Token)match(input,END,FOLLOW_END_in_functionDef242);  
            stream_END.add(END10);

            FUNCTION11=(Token)match(input,FUNCTION,FOLLOW_FUNCTION_in_functionDef244);  
            stream_FUNCTION.add(FUNCTION11);


            }



            // AST REWRITE
            // elements: e, id, s, par, r, p
            // token labels: id
            // rule labels: retval, e, s, r
            // token list labels: p
            // rule list labels: par
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_id=new RewriteRuleTokenStream(adaptor,"token id",id);
            RewriteRuleTokenStream stream_p=new RewriteRuleTokenStream(adaptor,"token p", list_p);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
            RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"rule e",e!=null?e.tree:null);
            RewriteRuleSubtreeStream stream_s=new RewriteRuleSubtreeStream(adaptor,"rule s",s!=null?s.tree:null);
            RewriteRuleSubtreeStream stream_r=new RewriteRuleSubtreeStream(adaptor,"rule r",r!=null?r.tree:null);
            RewriteRuleSubtreeStream stream_par=new RewriteRuleSubtreeStream(adaptor,"token par",list_par);
            root_0 = (CommonTree)adaptor.nil();
            // 76:6: -> ^( FUNCT ( $r)? DUMMY $id $s ( $e)? DUMMY ( $par $p)* )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:76:9: ^( FUNCT ( $r)? DUMMY $id $s ( $e)? DUMMY ( $par $p)* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(FUNCT, "FUNCT"), root_1);

                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:76:17: ( $r)?
                if ( stream_r.hasNext() ) {
                    adaptor.addChild(root_1, stream_r.nextTree());

                }
                stream_r.reset();
                adaptor.addChild(root_1, (CommonTree)adaptor.create(DUMMY, "DUMMY"));
                adaptor.addChild(root_1, stream_id.nextNode());
                adaptor.addChild(root_1, stream_s.nextTree());
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:76:36: ( $e)?
                if ( stream_e.hasNext() ) {
                    adaptor.addChild(root_1, stream_e.nextTree());

                }
                stream_e.reset();
                adaptor.addChild(root_1, (CommonTree)adaptor.create(DUMMY, "DUMMY"));
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:76:46: ( $par $p)*
                while ( stream_par.hasNext()||stream_p.hasNext() ) {
                    adaptor.addChild(root_1, stream_par.nextTree());
                    adaptor.addChild(root_1, stream_p.nextNode());

                }
                stream_par.reset();
                stream_p.reset();

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "functionDef"

    public static class slist_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "slist"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:80:1: slist : ( statement )+ -> ^( BLOCK ( statement )+ ) ;
    public final ProceduralParser.slist_return slist() throws RecognitionException {
        ProceduralParser.slist_return retval = new ProceduralParser.slist_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        ProceduralParser.statement_return statement12 = null;


        RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:80:6: ( ( statement )+ -> ^( BLOCK ( statement )+ ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:81:4: ( statement )+
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:81:4: ( statement )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==ID||LA7_0==PRINT||LA7_0==DECLARE||(LA7_0>=CALL && LA7_0<=IF)||LA7_0==WHILE) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:81:4: statement
            	    {
            	    pushFollow(FOLLOW_statement_in_slist297);
            	    statement12=statement();

            	    state._fsp--;

            	    stream_statement.add(statement12.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);



            // AST REWRITE
            // elements: statement
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 81:15: -> ^( BLOCK ( statement )+ )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:81:18: ^( BLOCK ( statement )+ )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, "BLOCK"), root_1);

                if ( !(stream_statement.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_statement.hasNext() ) {
                    adaptor.addChild(root_1, stream_statement.nextTree());

                }
                stream_statement.reset();

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "slist"

    public static class types_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "types"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:85:1: types : ( STR | INT | DBL | BLN ) ;
    public final ProceduralParser.types_return types() throws RecognitionException {
        ProceduralParser.types_return retval = new ProceduralParser.types_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set13=null;

        CommonTree set13_tree=null;

        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:85:6: ( ( STR | INT | DBL | BLN ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:86:1: ( STR | INT | DBL | BLN )
            {
            root_0 = (CommonTree)adaptor.nil();

            set13=(Token)input.LT(1);
            if ( (input.LA(1)>=STR && input.LA(1)<=BLN) ) {
                input.consume();
                adaptor.addChild(root_0, (CommonTree)adaptor.create(set13));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "types"

    public static class statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "statement"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:95:1: statement : ( print -> print | assignment -> assignment | declareVars -> declareVars | callSub -> callSub | ifStatement -> ifStatement | whileLoop -> whileLoop );
    public final ProceduralParser.statement_return statement() throws RecognitionException {
        ProceduralParser.statement_return retval = new ProceduralParser.statement_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        ProceduralParser.print_return print14 = null;

        ProceduralParser.assignment_return assignment15 = null;

        ProceduralParser.declareVars_return declareVars16 = null;

        ProceduralParser.callSub_return callSub17 = null;

        ProceduralParser.ifStatement_return ifStatement18 = null;

        ProceduralParser.whileLoop_return whileLoop19 = null;


        RewriteRuleSubtreeStream stream_declareVars=new RewriteRuleSubtreeStream(adaptor,"rule declareVars");
        RewriteRuleSubtreeStream stream_callSub=new RewriteRuleSubtreeStream(adaptor,"rule callSub");
        RewriteRuleSubtreeStream stream_assignment=new RewriteRuleSubtreeStream(adaptor,"rule assignment");
        RewriteRuleSubtreeStream stream_whileLoop=new RewriteRuleSubtreeStream(adaptor,"rule whileLoop");
        RewriteRuleSubtreeStream stream_print=new RewriteRuleSubtreeStream(adaptor,"rule print");
        RewriteRuleSubtreeStream stream_ifStatement=new RewriteRuleSubtreeStream(adaptor,"rule ifStatement");
         paraphrases.push("in statement"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:98:1: ( print -> print | assignment -> assignment | declareVars -> declareVars | callSub -> callSub | ifStatement -> ifStatement | whileLoop -> whileLoop )
            int alt8=6;
            switch ( input.LA(1) ) {
            case PRINT:
                {
                alt8=1;
                }
                break;
            case ID:
                {
                alt8=2;
                }
                break;
            case DECLARE:
                {
                alt8=3;
                }
                break;
            case CALL:
                {
                alt8=4;
                }
                break;
            case IF:
                {
                alt8=5;
                }
                break;
            case WHILE:
                {
                alt8=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:99:1: print
                    {
                    pushFollow(FOLLOW_print_in_statement356);
                    print14=print();

                    state._fsp--;

                    stream_print.add(print14.getTree());


                    // AST REWRITE
                    // elements: print
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 99:7: -> print
                    {
                        adaptor.addChild(root_0, stream_print.nextTree());

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:101:1: assignment
                    {
                    pushFollow(FOLLOW_assignment_in_statement364);
                    assignment15=assignment();

                    state._fsp--;

                    stream_assignment.add(assignment15.getTree());


                    // AST REWRITE
                    // elements: assignment
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 101:12: -> assignment
                    {
                        adaptor.addChild(root_0, stream_assignment.nextTree());

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:103:1: declareVars
                    {
                    pushFollow(FOLLOW_declareVars_in_statement372);
                    declareVars16=declareVars();

                    state._fsp--;

                    stream_declareVars.add(declareVars16.getTree());


                    // AST REWRITE
                    // elements: declareVars
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 103:13: -> declareVars
                    {
                        adaptor.addChild(root_0, stream_declareVars.nextTree());

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 4 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:105:1: callSub
                    {
                    pushFollow(FOLLOW_callSub_in_statement380);
                    callSub17=callSub();

                    state._fsp--;

                    stream_callSub.add(callSub17.getTree());


                    // AST REWRITE
                    // elements: callSub
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 105:9: -> callSub
                    {
                        adaptor.addChild(root_0, stream_callSub.nextTree());

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 5 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:107:1: ifStatement
                    {
                    pushFollow(FOLLOW_ifStatement_in_statement388);
                    ifStatement18=ifStatement();

                    state._fsp--;

                    stream_ifStatement.add(ifStatement18.getTree());


                    // AST REWRITE
                    // elements: ifStatement
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 107:13: -> ifStatement
                    {
                        adaptor.addChild(root_0, stream_ifStatement.nextTree());

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 6 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:109:1: whileLoop
                    {
                    pushFollow(FOLLOW_whileLoop_in_statement396);
                    whileLoop19=whileLoop();

                    state._fsp--;

                    stream_whileLoop.add(whileLoop19.getTree());


                    // AST REWRITE
                    // elements: whileLoop
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 109:11: -> whileLoop
                    {
                        adaptor.addChild(root_0, stream_whileLoop.nextTree());

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "statement"

    public static class print_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "print"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:113:1: print : PRINT expression -> ^( PRINT expression ) ;
    public final ProceduralParser.print_return print() throws RecognitionException {
        ProceduralParser.print_return retval = new ProceduralParser.print_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token PRINT20=null;
        ProceduralParser.expression_return expression21 = null;


        CommonTree PRINT20_tree=null;
        RewriteRuleTokenStream stream_PRINT=new RewriteRuleTokenStream(adaptor,"token PRINT");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
         paraphrases.push("in print statement"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:116:1: ( PRINT expression -> ^( PRINT expression ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:117:3: PRINT expression
            {
            PRINT20=(Token)match(input,PRINT,FOLLOW_PRINT_in_print423);  
            stream_PRINT.add(PRINT20);

            pushFollow(FOLLOW_expression_in_print425);
            expression21=expression();

            state._fsp--;

            stream_expression.add(expression21.getTree());


            // AST REWRITE
            // elements: expression, PRINT
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 117:20: -> ^( PRINT expression )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:117:23: ^( PRINT expression )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(stream_PRINT.nextNode(), root_1);

                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "print"

    public static class assignment_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "assignment"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:121:1: assignment : id= ID EQ expression -> ^( ASSIGN $id expression ) ;
    public final ProceduralParser.assignment_return assignment() throws RecognitionException {
        ProceduralParser.assignment_return retval = new ProceduralParser.assignment_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token id=null;
        Token EQ22=null;
        ProceduralParser.expression_return expression23 = null;


        CommonTree id_tree=null;
        CommonTree EQ22_tree=null;
        RewriteRuleTokenStream stream_EQ=new RewriteRuleTokenStream(adaptor,"token EQ");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
         paraphrases.push("in assignment"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:124:1: (id= ID EQ expression -> ^( ASSIGN $id expression ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:125:1: id= ID EQ expression
            {
            id=(Token)match(input,ID,FOLLOW_ID_in_assignment456);  
            stream_ID.add(id);

            EQ22=(Token)match(input,EQ,FOLLOW_EQ_in_assignment458);  
            stream_EQ.add(EQ22);

            pushFollow(FOLLOW_expression_in_assignment460);
            expression23=expression();

            state._fsp--;

            stream_expression.add(expression23.getTree());


            // AST REWRITE
            // elements: id, expression
            // token labels: id
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_id=new RewriteRuleTokenStream(adaptor,"token id",id);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 125:21: -> ^( ASSIGN $id expression )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:125:24: ^( ASSIGN $id expression )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ASSIGN, "ASSIGN"), root_1);

                adaptor.addChild(root_1, stream_id.nextNode());
                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "assignment"

    public static class declareVars_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "declareVars"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:129:1: declareVars : DECLARE varName= ID AS t= types -> ^( VARDECLARE $varName $t) ;
    public final ProceduralParser.declareVars_return declareVars() throws RecognitionException {
        ProceduralParser.declareVars_return retval = new ProceduralParser.declareVars_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token varName=null;
        Token DECLARE24=null;
        Token AS25=null;
        ProceduralParser.types_return t = null;


        CommonTree varName_tree=null;
        CommonTree DECLARE24_tree=null;
        CommonTree AS25_tree=null;
        RewriteRuleTokenStream stream_AS=new RewriteRuleTokenStream(adaptor,"token AS");
        RewriteRuleTokenStream stream_DECLARE=new RewriteRuleTokenStream(adaptor,"token DECLARE");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleSubtreeStream stream_types=new RewriteRuleSubtreeStream(adaptor,"rule types");
         paraphrases.push("in variable declaration"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:132:1: ( DECLARE varName= ID AS t= types -> ^( VARDECLARE $varName $t) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:133:1: DECLARE varName= ID AS t= types
            {
            DECLARE24=(Token)match(input,DECLARE,FOLLOW_DECLARE_in_declareVars492);  
            stream_DECLARE.add(DECLARE24);

            varName=(Token)match(input,ID,FOLLOW_ID_in_declareVars496);  
            stream_ID.add(varName);

            AS25=(Token)match(input,AS,FOLLOW_AS_in_declareVars498);  
            stream_AS.add(AS25);

            pushFollow(FOLLOW_types_in_declareVars502);
            t=types();

            state._fsp--;

            stream_types.add(t.getTree());


            // AST REWRITE
            // elements: t, varName
            // token labels: varName
            // rule labels: retval, t
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_varName=new RewriteRuleTokenStream(adaptor,"token varName",varName);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
            RewriteRuleSubtreeStream stream_t=new RewriteRuleSubtreeStream(adaptor,"rule t",t!=null?t.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 133:31: -> ^( VARDECLARE $varName $t)
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:133:34: ^( VARDECLARE $varName $t)
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VARDECLARE, "VARDECLARE"), root_1);

                adaptor.addChild(root_1, stream_varName.nextNode());
                adaptor.addChild(root_1, stream_t.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "declareVars"

    public static class callSub_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "callSub"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:137:1: callSub : CALL ID ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )? -> ^( CALLVOID ID ( LPAREN )? ( $e)* ) ;
    public final ProceduralParser.callSub_return callSub() throws RecognitionException {
        ProceduralParser.callSub_return retval = new ProceduralParser.callSub_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token CALL26=null;
        Token ID27=null;
        Token LPAREN28=null;
        Token COMMA29=null;
        Token RPAREN30=null;
        List list_e=null;
        RuleReturnScope e = null;
        CommonTree CALL26_tree=null;
        CommonTree ID27_tree=null;
        CommonTree LPAREN28_tree=null;
        CommonTree COMMA29_tree=null;
        CommonTree RPAREN30_tree=null;
        RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
        RewriteRuleTokenStream stream_CALL=new RewriteRuleTokenStream(adaptor,"token CALL");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
         paraphrases.push("in variable declaration"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:140:1: ( CALL ID ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )? -> ^( CALLVOID ID ( LPAREN )? ( $e)* ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:1: CALL ID ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )?
            {
            CALL26=(Token)match(input,CALL,FOLLOW_CALL_in_callSub536);  
            stream_CALL.add(CALL26);

            ID27=(Token)match(input,ID,FOLLOW_ID_in_callSub538);  
            stream_ID.add(ID27);

            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:9: ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==LPAREN) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:10: LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN
                    {
                    LPAREN28=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_callSub541);  
                    stream_LPAREN.add(LPAREN28);

                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:18: (e+= expression )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( ((LA9_0>=ID && LA9_0<=LPAREN)||(LA9_0>=BOOLEAN && LA9_0<=DOUBLE)||LA9_0==MINUS) ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:18: e+= expression
                            {
                            pushFollow(FOLLOW_expression_in_callSub545);
                            e=expression();

                            state._fsp--;

                            stream_expression.add(e.getTree());
                            if (list_e==null) list_e=new ArrayList();
                            list_e.add(e.getTree());


                            }
                            break;

                    }

                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:32: ( COMMA e+= expression )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==COMMA) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:33: COMMA e+= expression
                    	    {
                    	    COMMA29=(Token)match(input,COMMA,FOLLOW_COMMA_in_callSub549);  
                    	    stream_COMMA.add(COMMA29);

                    	    pushFollow(FOLLOW_expression_in_callSub553);
                    	    e=expression();

                    	    state._fsp--;

                    	    stream_expression.add(e.getTree());
                    	    if (list_e==null) list_e=new ArrayList();
                    	    list_e.add(e.getTree());


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    RPAREN30=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_callSub558);  
                    stream_RPAREN.add(RPAREN30);


                    }
                    break;

            }



            // AST REWRITE
            // elements: LPAREN, e, ID
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: e
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
            RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"token e",list_e);
            root_0 = (CommonTree)adaptor.nil();
            // 141:65: -> ^( CALLVOID ID ( LPAREN )? ( $e)* )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:68: ^( CALLVOID ID ( LPAREN )? ( $e)* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CALLVOID, "CALLVOID"), root_1);

                adaptor.addChild(root_1, stream_ID.nextNode());
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:82: ( LPAREN )?
                if ( stream_LPAREN.hasNext() ) {
                    adaptor.addChild(root_1, stream_LPAREN.nextNode());

                }
                stream_LPAREN.reset();
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:141:91: ( $e)*
                while ( stream_e.hasNext() ) {
                    adaptor.addChild(root_1, stream_e.nextTree());

                }
                stream_e.reset();

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "callSub"

    public static class ifStatement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "ifStatement"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:145:1: ifStatement : IF mainIf= expression THEN (mainSlist= slist ) ( ELSE elseSlist= slist )? END IF -> ^( IFELSE $mainIf $mainSlist ( $elseSlist)? ) ;
    public final ProceduralParser.ifStatement_return ifStatement() throws RecognitionException {
        ProceduralParser.ifStatement_return retval = new ProceduralParser.ifStatement_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token IF31=null;
        Token THEN32=null;
        Token ELSE33=null;
        Token END34=null;
        Token IF35=null;
        ProceduralParser.expression_return mainIf = null;

        ProceduralParser.slist_return mainSlist = null;

        ProceduralParser.slist_return elseSlist = null;


        CommonTree IF31_tree=null;
        CommonTree THEN32_tree=null;
        CommonTree ELSE33_tree=null;
        CommonTree END34_tree=null;
        CommonTree IF35_tree=null;
        RewriteRuleTokenStream stream_THEN=new RewriteRuleTokenStream(adaptor,"token THEN");
        RewriteRuleTokenStream stream_END=new RewriteRuleTokenStream(adaptor,"token END");
        RewriteRuleTokenStream stream_IF=new RewriteRuleTokenStream(adaptor,"token IF");
        RewriteRuleTokenStream stream_ELSE=new RewriteRuleTokenStream(adaptor,"token ELSE");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_slist=new RewriteRuleSubtreeStream(adaptor,"rule slist");
         paraphrases.push("in if-else statement"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:148:1: ( IF mainIf= expression THEN (mainSlist= slist ) ( ELSE elseSlist= slist )? END IF -> ^( IFELSE $mainIf $mainSlist ( $elseSlist)? ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:149:3: IF mainIf= expression THEN (mainSlist= slist ) ( ELSE elseSlist= slist )? END IF
            {
            IF31=(Token)match(input,IF,FOLLOW_IF_in_ifStatement599);  
            stream_IF.add(IF31);

            pushFollow(FOLLOW_expression_in_ifStatement603);
            mainIf=expression();

            state._fsp--;

            stream_expression.add(mainIf.getTree());
            THEN32=(Token)match(input,THEN,FOLLOW_THEN_in_ifStatement606);  
            stream_THEN.add(THEN32);

            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:149:31: (mainSlist= slist )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:149:32: mainSlist= slist
            {
            pushFollow(FOLLOW_slist_in_ifStatement612);
            mainSlist=slist();

            state._fsp--;

            stream_slist.add(mainSlist.getTree());

            }

            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:150:3: ( ELSE elseSlist= slist )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==ELSE) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:150:4: ELSE elseSlist= slist
                    {
                    ELSE33=(Token)match(input,ELSE,FOLLOW_ELSE_in_ifStatement618);  
                    stream_ELSE.add(ELSE33);

                    pushFollow(FOLLOW_slist_in_ifStatement622);
                    elseSlist=slist();

                    state._fsp--;

                    stream_slist.add(elseSlist.getTree());

                    }
                    break;

            }

            END34=(Token)match(input,END,FOLLOW_END_in_ifStatement629);  
            stream_END.add(END34);

            IF35=(Token)match(input,IF,FOLLOW_IF_in_ifStatement631);  
            stream_IF.add(IF35);



            // AST REWRITE
            // elements: mainSlist, mainIf, elseSlist
            // token labels: 
            // rule labels: elseSlist, retval, mainSlist, mainIf
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_elseSlist=new RewriteRuleSubtreeStream(adaptor,"rule elseSlist",elseSlist!=null?elseSlist.tree:null);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
            RewriteRuleSubtreeStream stream_mainSlist=new RewriteRuleSubtreeStream(adaptor,"rule mainSlist",mainSlist!=null?mainSlist.tree:null);
            RewriteRuleSubtreeStream stream_mainIf=new RewriteRuleSubtreeStream(adaptor,"rule mainIf",mainIf!=null?mainIf.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 152:3: -> ^( IFELSE $mainIf $mainSlist ( $elseSlist)? )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:152:6: ^( IFELSE $mainIf $mainSlist ( $elseSlist)? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(IFELSE, "IFELSE"), root_1);

                adaptor.addChild(root_1, stream_mainIf.nextTree());
                adaptor.addChild(root_1, stream_mainSlist.nextTree());
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:152:34: ( $elseSlist)?
                if ( stream_elseSlist.hasNext() ) {
                    adaptor.addChild(root_1, stream_elseSlist.nextTree());

                }
                stream_elseSlist.reset();

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "ifStatement"

    public static class whileLoop_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "whileLoop"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:156:1: whileLoop : WHILE expression slist END WHILE -> ^( WHILE expression slist ) ;
    public final ProceduralParser.whileLoop_return whileLoop() throws RecognitionException {
        ProceduralParser.whileLoop_return retval = new ProceduralParser.whileLoop_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token WHILE36=null;
        Token END39=null;
        Token WHILE40=null;
        ProceduralParser.expression_return expression37 = null;

        ProceduralParser.slist_return slist38 = null;


        CommonTree WHILE36_tree=null;
        CommonTree END39_tree=null;
        CommonTree WHILE40_tree=null;
        RewriteRuleTokenStream stream_WHILE=new RewriteRuleTokenStream(adaptor,"token WHILE");
        RewriteRuleTokenStream stream_END=new RewriteRuleTokenStream(adaptor,"token END");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_slist=new RewriteRuleSubtreeStream(adaptor,"rule slist");
         paraphrases.push("in while loop "); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:159:1: ( WHILE expression slist END WHILE -> ^( WHILE expression slist ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:160:3: WHILE expression slist END WHILE
            {
            WHILE36=(Token)match(input,WHILE,FOLLOW_WHILE_in_whileLoop674);  
            stream_WHILE.add(WHILE36);

            pushFollow(FOLLOW_expression_in_whileLoop676);
            expression37=expression();

            state._fsp--;

            stream_expression.add(expression37.getTree());
            pushFollow(FOLLOW_slist_in_whileLoop680);
            slist38=slist();

            state._fsp--;

            stream_slist.add(slist38.getTree());
            END39=(Token)match(input,END,FOLLOW_END_in_whileLoop684);  
            stream_END.add(END39);

            WHILE40=(Token)match(input,WHILE,FOLLOW_WHILE_in_whileLoop686);  
            stream_WHILE.add(WHILE40);



            // AST REWRITE
            // elements: slist, WHILE, expression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 163:3: -> ^( WHILE expression slist )
            {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:163:6: ^( WHILE expression slist )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(stream_WHILE.nextNode(), root_1);

                adaptor.addChild(root_1, stream_expression.nextTree());
                adaptor.addChild(root_1, stream_slist.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "whileLoop"

    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "atom"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:173:1: atom : ( BOOLEAN | STRING | INTEGER | DOUBLE | ID ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )? -> ^( VAR_OR_FUNCTCALL ID ( LPAREN )? ( $e)* ) | LPAREN expression RPAREN -> ^( EXPRENTER expression ) );
    public final ProceduralParser.atom_return atom() throws RecognitionException {
        ProceduralParser.atom_return retval = new ProceduralParser.atom_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token BOOLEAN41=null;
        Token STRING42=null;
        Token INTEGER43=null;
        Token DOUBLE44=null;
        Token ID45=null;
        Token LPAREN46=null;
        Token COMMA47=null;
        Token RPAREN48=null;
        Token LPAREN49=null;
        Token RPAREN51=null;
        List list_e=null;
        ProceduralParser.expression_return expression50 = null;

        RuleReturnScope e = null;
        CommonTree BOOLEAN41_tree=null;
        CommonTree STRING42_tree=null;
        CommonTree INTEGER43_tree=null;
        CommonTree DOUBLE44_tree=null;
        CommonTree ID45_tree=null;
        CommonTree LPAREN46_tree=null;
        CommonTree COMMA47_tree=null;
        CommonTree RPAREN48_tree=null;
        CommonTree LPAREN49_tree=null;
        CommonTree RPAREN51_tree=null;
        RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:173:5: ( BOOLEAN | STRING | INTEGER | DOUBLE | ID ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )? -> ^( VAR_OR_FUNCTCALL ID ( LPAREN )? ( $e)* ) | LPAREN expression RPAREN -> ^( EXPRENTER expression ) )
            int alt16=6;
            switch ( input.LA(1) ) {
            case BOOLEAN:
                {
                alt16=1;
                }
                break;
            case STRING:
                {
                alt16=2;
                }
                break;
            case INTEGER:
                {
                alt16=3;
                }
                break;
            case DOUBLE:
                {
                alt16=4;
                }
                break;
            case ID:
                {
                alt16=5;
                }
                break;
            case LPAREN:
                {
                alt16=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:174:3: BOOLEAN
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    BOOLEAN41=(Token)match(input,BOOLEAN,FOLLOW_BOOLEAN_in_atom717); 
                    BOOLEAN41_tree = (CommonTree)adaptor.create(BOOLEAN41);
                    adaptor.addChild(root_0, BOOLEAN41_tree);


                    }
                    break;
                case 2 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:176:3: STRING
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    STRING42=(Token)match(input,STRING,FOLLOW_STRING_in_atom725); 
                    STRING42_tree = (CommonTree)adaptor.create(STRING42);
                    adaptor.addChild(root_0, STRING42_tree);


                    }
                    break;
                case 3 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:178:3: INTEGER
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    INTEGER43=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_atom735); 
                    INTEGER43_tree = (CommonTree)adaptor.create(INTEGER43);
                    adaptor.addChild(root_0, INTEGER43_tree);


                    }
                    break;
                case 4 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:180:3: DOUBLE
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    DOUBLE44=(Token)match(input,DOUBLE,FOLLOW_DOUBLE_in_atom744); 
                    DOUBLE44_tree = (CommonTree)adaptor.create(DOUBLE44);
                    adaptor.addChild(root_0, DOUBLE44_tree);


                    }
                    break;
                case 5 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:3: ID ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )?
                    {
                    ID45=(Token)match(input,ID,FOLLOW_ID_in_atom753);  
                    stream_ID.add(ID45);

                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:6: ( LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==LPAREN) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:7: LPAREN (e+= expression )? ( COMMA e+= expression )* RPAREN
                            {
                            LPAREN46=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_atom756);  
                            stream_LPAREN.add(LPAREN46);

                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:15: (e+= expression )?
                            int alt13=2;
                            int LA13_0 = input.LA(1);

                            if ( ((LA13_0>=ID && LA13_0<=LPAREN)||(LA13_0>=BOOLEAN && LA13_0<=DOUBLE)||LA13_0==MINUS) ) {
                                alt13=1;
                            }
                            switch (alt13) {
                                case 1 :
                                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:15: e+= expression
                                    {
                                    pushFollow(FOLLOW_expression_in_atom760);
                                    e=expression();

                                    state._fsp--;

                                    stream_expression.add(e.getTree());
                                    if (list_e==null) list_e=new ArrayList();
                                    list_e.add(e.getTree());


                                    }
                                    break;

                            }

                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:29: ( COMMA e+= expression )*
                            loop14:
                            do {
                                int alt14=2;
                                int LA14_0 = input.LA(1);

                                if ( (LA14_0==COMMA) ) {
                                    alt14=1;
                                }


                                switch (alt14) {
                            	case 1 :
                            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:30: COMMA e+= expression
                            	    {
                            	    COMMA47=(Token)match(input,COMMA,FOLLOW_COMMA_in_atom764);  
                            	    stream_COMMA.add(COMMA47);

                            	    pushFollow(FOLLOW_expression_in_atom768);
                            	    e=expression();

                            	    state._fsp--;

                            	    stream_expression.add(e.getTree());
                            	    if (list_e==null) list_e=new ArrayList();
                            	    list_e.add(e.getTree());


                            	    }
                            	    break;

                            	default :
                            	    break loop14;
                                }
                            } while (true);

                            RPAREN48=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_atom773);  
                            stream_RPAREN.add(RPAREN48);


                            }
                            break;

                    }



                    // AST REWRITE
                    // elements: e, ID, LPAREN
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: e
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"token e",list_e);
                    root_0 = (CommonTree)adaptor.nil();
                    // 182:62: -> ^( VAR_OR_FUNCTCALL ID ( LPAREN )? ( $e)* )
                    {
                        // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:65: ^( VAR_OR_FUNCTCALL ID ( LPAREN )? ( $e)* )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VAR_OR_FUNCTCALL, "VAR_OR_FUNCTCALL"), root_1);

                        adaptor.addChild(root_1, stream_ID.nextNode());
                        // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:87: ( LPAREN )?
                        if ( stream_LPAREN.hasNext() ) {
                            adaptor.addChild(root_1, stream_LPAREN.nextNode());

                        }
                        stream_LPAREN.reset();
                        // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:182:96: ( $e)*
                        while ( stream_e.hasNext() ) {
                            adaptor.addChild(root_1, stream_e.nextTree());

                        }
                        stream_e.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 6 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:184:3: LPAREN expression RPAREN
                    {
                    LPAREN49=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_atom799);  
                    stream_LPAREN.add(LPAREN49);

                    pushFollow(FOLLOW_expression_in_atom801);
                    expression50=expression();

                    state._fsp--;

                    stream_expression.add(expression50.getTree());
                    RPAREN51=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_atom803);  
                    stream_RPAREN.add(RPAREN51);



                    // AST REWRITE
                    // elements: expression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 184:28: -> ^( EXPRENTER expression )
                    {
                        // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:184:31: ^( EXPRENTER expression )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(EXPRENTER, "EXPRENTER"), root_1);

                        adaptor.addChild(root_1, stream_expression.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "atom"

    public static class exponentiation_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "exponentiation"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:187:1: exponentiation : atom ( EXP atom )* ;
    public final ProceduralParser.exponentiation_return exponentiation() throws RecognitionException {
        ProceduralParser.exponentiation_return retval = new ProceduralParser.exponentiation_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token EXP53=null;
        ProceduralParser.atom_return atom52 = null;

        ProceduralParser.atom_return atom54 = null;


        CommonTree EXP53_tree=null;

        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:187:15: ( atom ( EXP atom )* )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:188:3: atom ( EXP atom )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_atom_in_exponentiation821);
            atom52=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom52.getTree());
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:188:8: ( EXP atom )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==EXP) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:188:9: EXP atom
            	    {
            	    EXP53=(Token)match(input,EXP,FOLLOW_EXP_in_exponentiation824); 
            	    EXP53_tree = (CommonTree)adaptor.create(EXP53);
            	    root_0 = (CommonTree)adaptor.becomeRoot(EXP53_tree, root_0);

            	    pushFollow(FOLLOW_atom_in_exponentiation827);
            	    atom54=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom54.getTree());

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "exponentiation"

    public static class unary_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "unary"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:191:1: unary : (m= MINUS )? exponentiation -> {(m != null)}? ^( UNARY exponentiation ) -> exponentiation ;
    public final ProceduralParser.unary_return unary() throws RecognitionException {
        ProceduralParser.unary_return retval = new ProceduralParser.unary_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token m=null;
        ProceduralParser.exponentiation_return exponentiation55 = null;


        CommonTree m_tree=null;
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
        RewriteRuleSubtreeStream stream_exponentiation=new RewriteRuleSubtreeStream(adaptor,"rule exponentiation");
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:191:6: ( (m= MINUS )? exponentiation -> {(m != null)}? ^( UNARY exponentiation ) -> exponentiation )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:192:3: (m= MINUS )? exponentiation
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:192:3: (m= MINUS )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==MINUS) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:192:4: m= MINUS
                    {
                    m=(Token)match(input,MINUS,FOLLOW_MINUS_in_unary842);  
                    stream_MINUS.add(m);


                    }
                    break;

            }

            pushFollow(FOLLOW_exponentiation_in_unary846);
            exponentiation55=exponentiation();

            state._fsp--;

            stream_exponentiation.add(exponentiation55.getTree());


            // AST REWRITE
            // elements: exponentiation, exponentiation
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 193:3: -> {(m != null)}? ^( UNARY exponentiation )
            if ((m != null)) {
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:193:20: ^( UNARY exponentiation )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(UNARY, "UNARY"), root_1);

                adaptor.addChild(root_1, stream_exponentiation.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }
            else // 194:3: -> exponentiation
            {
                adaptor.addChild(root_0, stream_exponentiation.nextTree());

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "unary"

    public static class multiply_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "multiply"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:197:1: multiply : unary ( ( MULT | DIV | MOD ) unary )* ;
    public final ProceduralParser.multiply_return multiply() throws RecognitionException {
        ProceduralParser.multiply_return retval = new ProceduralParser.multiply_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set57=null;
        ProceduralParser.unary_return unary56 = null;

        ProceduralParser.unary_return unary58 = null;


        CommonTree set57_tree=null;

        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:197:9: ( unary ( ( MULT | DIV | MOD ) unary )* )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:198:3: unary ( ( MULT | DIV | MOD ) unary )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_unary_in_multiply875);
            unary56=unary();

            state._fsp--;

            adaptor.addChild(root_0, unary56.getTree());
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:198:9: ( ( MULT | DIV | MOD ) unary )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=MULT && LA19_0<=MOD)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:198:11: ( MULT | DIV | MOD ) unary
            	    {
            	    set57=(Token)input.LT(1);
            	    set57=(Token)input.LT(1);
            	    if ( (input.LA(1)>=MULT && input.LA(1)<=MOD) ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(set57), root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }

            	    pushFollow(FOLLOW_unary_in_multiply892);
            	    unary58=unary();

            	    state._fsp--;

            	    adaptor.addChild(root_0, unary58.getTree());

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "multiply"

    public static class addition_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "addition"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:201:1: addition : multiply ( ( PLUS | MINUS ) multiply )* ;
    public final ProceduralParser.addition_return addition() throws RecognitionException {
        ProceduralParser.addition_return retval = new ProceduralParser.addition_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set60=null;
        ProceduralParser.multiply_return multiply59 = null;

        ProceduralParser.multiply_return multiply61 = null;


        CommonTree set60_tree=null;

        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:201:9: ( multiply ( ( PLUS | MINUS ) multiply )* )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:202:3: multiply ( ( PLUS | MINUS ) multiply )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_multiply_in_addition904);
            multiply59=multiply();

            state._fsp--;

            adaptor.addChild(root_0, multiply59.getTree());
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:202:12: ( ( PLUS | MINUS ) multiply )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==MINUS||LA20_0==PLUS) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:202:14: ( PLUS | MINUS ) multiply
            	    {
            	    set60=(Token)input.LT(1);
            	    set60=(Token)input.LT(1);
            	    if ( input.LA(1)==MINUS||input.LA(1)==PLUS ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(set60), root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }

            	    pushFollow(FOLLOW_multiply_in_addition917);
            	    multiply61=multiply();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multiply61.getTree());

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "addition"

    public static class expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "expression"
    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:205:1: expression : addition ( ( EQ | NOTEQ | LT | LTEQ | GTEQ | GT ) addition )* ;
    public final ProceduralParser.expression_return expression() throws RecognitionException {
        ProceduralParser.expression_return retval = new ProceduralParser.expression_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set63=null;
        ProceduralParser.addition_return addition62 = null;

        ProceduralParser.addition_return addition64 = null;


        CommonTree set63_tree=null;

         paraphrases.push("in expression"); 
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:208:1: ( addition ( ( EQ | NOTEQ | LT | LTEQ | GTEQ | GT ) addition )* )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:209:3: addition ( ( EQ | NOTEQ | LT | LTEQ | GTEQ | GT ) addition )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_addition_in_expression941);
            addition62=addition();

            state._fsp--;

            adaptor.addChild(root_0, addition62.getTree());
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:209:12: ( ( EQ | NOTEQ | LT | LTEQ | GTEQ | GT ) addition )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==EQ||(LA21_0>=NOTEQ && LA21_0<=GT)) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:209:14: ( EQ | NOTEQ | LT | LTEQ | GTEQ | GT ) addition
            	    {
            	    set63=(Token)input.LT(1);
            	    set63=(Token)input.LT(1);
            	    if ( input.LA(1)==EQ||(input.LA(1)>=NOTEQ && input.LA(1)<=GT) ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(set63), root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }

            	    pushFollow(FOLLOW_addition_in_expression971);
            	    addition64=addition();

            	    state._fsp--;

            	    adaptor.addChild(root_0, addition64.getTree());

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

             paraphrases.pop(); 
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "expression"

    // Delegated rules


 

    public static final BitSet FOLLOW_functionDef_in_program114 = new BitSet(new long[]{0x000000026A00C000L});
    public static final BitSet FOLLOW_block_in_program117 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_program119 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_slist_in_block141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FUNCTION_in_functionDef168 = new BitSet(new long[]{0x0000000001E08000L});
    public static final BitSet FOLLOW_types_in_functionDef172 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ID_in_functionDef177 = new BitSet(new long[]{0x000000026A018000L});
    public static final BitSet FOLLOW_LPAREN_in_functionDef182 = new BitSet(new long[]{0x0000000001E40000L});
    public static final BitSet FOLLOW_types_in_functionDef188 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ID_in_functionDef192 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_COMMA_in_functionDef195 = new BitSet(new long[]{0x0000000001E00000L});
    public static final BitSet FOLLOW_types_in_functionDef199 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ID_in_functionDef203 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_RPAREN_in_functionDef210 = new BitSet(new long[]{0x000000026A008000L});
    public static final BitSet FOLLOW_slist_in_functionDef221 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_RETURN_in_functionDef227 = new BitSet(new long[]{0x000000BC00118000L});
    public static final BitSet FOLLOW_expression_in_functionDef231 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_END_in_functionDef242 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_FUNCTION_in_functionDef244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_slist297 = new BitSet(new long[]{0x000000026A008002L});
    public static final BitSet FOLLOW_set_in_types316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_print_in_statement356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_in_statement364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declareVars_in_statement372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callSub_in_statement380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_whileLoop_in_statement396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PRINT_in_print423 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_print425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_assignment456 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_EQ_in_assignment458 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_assignment460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DECLARE_in_declareVars492 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ID_in_declareVars496 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_AS_in_declareVars498 = new BitSet(new long[]{0x0000000001E00000L});
    public static final BitSet FOLLOW_types_in_declareVars502 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CALL_in_callSub536 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ID_in_callSub538 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_LPAREN_in_callSub541 = new BitSet(new long[]{0x000000BC00078000L});
    public static final BitSet FOLLOW_expression_in_callSub545 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_COMMA_in_callSub549 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_callSub553 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_RPAREN_in_callSub558 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_ifStatement599 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_ifStatement603 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_THEN_in_ifStatement606 = new BitSet(new long[]{0x000000026A008000L});
    public static final BitSet FOLLOW_slist_in_ifStatement612 = new BitSet(new long[]{0x0000000100100000L});
    public static final BitSet FOLLOW_ELSE_in_ifStatement618 = new BitSet(new long[]{0x000000026A008000L});
    public static final BitSet FOLLOW_slist_in_ifStatement622 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_END_in_ifStatement629 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_IF_in_ifStatement631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WHILE_in_whileLoop674 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_whileLoop676 = new BitSet(new long[]{0x000000026A008000L});
    public static final BitSet FOLLOW_slist_in_whileLoop680 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_END_in_whileLoop684 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_WHILE_in_whileLoop686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BOOLEAN_in_atom717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_atom725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_atom735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOUBLE_in_atom744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_atom753 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_LPAREN_in_atom756 = new BitSet(new long[]{0x000000BC00078000L});
    public static final BitSet FOLLOW_expression_in_atom760 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_COMMA_in_atom764 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_atom768 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_RPAREN_in_atom773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_atom799 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_expression_in_atom801 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_RPAREN_in_atom803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_atom_in_exponentiation821 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_EXP_in_exponentiation824 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_atom_in_exponentiation827 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_MINUS_in_unary842 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_exponentiation_in_unary846 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_in_multiply875 = new BitSet(new long[]{0x0000070000000002L});
    public static final BitSet FOLLOW_set_in_multiply879 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_unary_in_multiply892 = new BitSet(new long[]{0x0000070000000002L});
    public static final BitSet FOLLOW_multiply_in_addition904 = new BitSet(new long[]{0x0000088000000002L});
    public static final BitSet FOLLOW_set_in_addition908 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_multiply_in_addition917 = new BitSet(new long[]{0x0000088000000002L});
    public static final BitSet FOLLOW_addition_in_expression941 = new BitSet(new long[]{0x0001F00004000002L});
    public static final BitSet FOLLOW_set_in_expression945 = new BitSet(new long[]{0x000000BC00018000L});
    public static final BitSet FOLLOW_addition_in_expression971 = new BitSet(new long[]{0x0001F00004000002L});

}