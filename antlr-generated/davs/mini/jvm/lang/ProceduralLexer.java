// $ANTLR 3.3 Nov 30, 2010 12:50:56 /home/davors/git/miniJVM/MiniJVM/src/Procedural.g 2012-10-07 12:13:09
  package davs.mini.jvm.lang;  

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class ProceduralLexer extends Lexer {
    public static final int EOF=-1;
    public static final int ASSIGN=4;
    public static final int BLOCK=5;
    public static final int CALLVOID=6;
    public static final int DUMMY=7;
    public static final int EXPRENTER=8;
    public static final int FUNCT=9;
    public static final int IFELSE=10;
    public static final int UNARY=11;
    public static final int VARDECLARE=12;
    public static final int VAR_OR_FUNCTCALL=13;
    public static final int FUNCTION=14;
    public static final int ID=15;
    public static final int LPAREN=16;
    public static final int COMMA=17;
    public static final int RPAREN=18;
    public static final int RETURN=19;
    public static final int END=20;
    public static final int STR=21;
    public static final int INT=22;
    public static final int DBL=23;
    public static final int BLN=24;
    public static final int PRINT=25;
    public static final int EQ=26;
    public static final int DECLARE=27;
    public static final int AS=28;
    public static final int CALL=29;
    public static final int IF=30;
    public static final int THEN=31;
    public static final int ELSE=32;
    public static final int WHILE=33;
    public static final int BOOLEAN=34;
    public static final int STRING=35;
    public static final int INTEGER=36;
    public static final int DOUBLE=37;
    public static final int EXP=38;
    public static final int MINUS=39;
    public static final int MULT=40;
    public static final int DIV=41;
    public static final int MOD=42;
    public static final int PLUS=43;
    public static final int NOTEQ=44;
    public static final int LT=45;
    public static final int LTEQ=46;
    public static final int GTEQ=47;
    public static final int GT=48;
    public static final int REMARK=49;
    public static final int NL=50;
    public static final int LETTER=51;
    public static final int DIGIT=52;
    public static final int COMMENT=53;
    public static final int WS=54;

    // delegates
    // delegators

    public ProceduralLexer() {;} 
    public ProceduralLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public ProceduralLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "/home/davors/git/miniJVM/MiniJVM/src/Procedural.g"; }

    // $ANTLR start "AS"
    public final void mAS() throws RecognitionException {
        try {
            int _type = AS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:220:11: ( 'AS' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:220:16: 'AS'
            {
            match("AS"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "AS"

    // $ANTLR start "CALL"
    public final void mCALL() throws RecognitionException {
        try {
            int _type = CALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:221:11: ( 'CALL' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:221:16: 'CALL'
            {
            match("CALL"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "CALL"

    // $ANTLR start "DECLARE"
    public final void mDECLARE() throws RecognitionException {
        try {
            int _type = DECLARE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:222:11: ( 'DECLARE' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:222:16: 'DECLARE'
            {
            match("DECLARE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DECLARE"

    // $ANTLR start "ELSE"
    public final void mELSE() throws RecognitionException {
        try {
            int _type = ELSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:223:11: ( 'ELSE' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:223:16: 'ELSE'
            {
            match("ELSE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ELSE"

    // $ANTLR start "END"
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:224:11: ( 'END' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:224:16: 'END'
            {
            match("END"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "FUNCTION"
    public final void mFUNCTION() throws RecognitionException {
        try {
            int _type = FUNCTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:225:11: ( 'FUNCTION' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:225:16: 'FUNCTION'
            {
            match("FUNCTION"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "FUNCTION"

    // $ANTLR start "IF"
    public final void mIF() throws RecognitionException {
        try {
            int _type = IF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:226:11: ( 'IF' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:226:16: 'IF'
            {
            match("IF"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IF"

    // $ANTLR start "MOD"
    public final void mMOD() throws RecognitionException {
        try {
            int _type = MOD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:227:11: ( 'MOD' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:227:16: 'MOD'
            {
            match("MOD"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "MOD"

    // $ANTLR start "PRINT"
    public final void mPRINT() throws RecognitionException {
        try {
            int _type = PRINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:228:11: ( 'PRINT' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:228:16: 'PRINT'
            {
            match("PRINT"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PRINT"

    // $ANTLR start "REMARK"
    public final void mREMARK() throws RecognitionException {
        try {
            int _type = REMARK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:229:11: ( '#' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:229:16: '#'
            {
            match('#'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REMARK"

    // $ANTLR start "RETURN"
    public final void mRETURN() throws RecognitionException {
        try {
            int _type = RETURN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:230:11: ( 'RETURN' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:230:16: 'RETURN'
            {
            match("RETURN"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RETURN"

    // $ANTLR start "THEN"
    public final void mTHEN() throws RecognitionException {
        try {
            int _type = THEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:231:11: ( 'THEN' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:231:16: 'THEN'
            {
            match("THEN"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "THEN"

    // $ANTLR start "WHILE"
    public final void mWHILE() throws RecognitionException {
        try {
            int _type = WHILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:232:11: ( 'WHILE' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:232:16: 'WHILE'
            {
            match("WHILE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WHILE"

    // $ANTLR start "LTEQ"
    public final void mLTEQ() throws RecognitionException {
        try {
            int _type = LTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:233:11: ( '<=' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:233:16: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LTEQ"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:234:11: ( '<' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:234:16: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "GTEQ"
    public final void mGTEQ() throws RecognitionException {
        try {
            int _type = GTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:235:11: ( '>=' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:235:16: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GTEQ"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:236:11: ( '>' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:236:16: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "EQ"
    public final void mEQ() throws RecognitionException {
        try {
            int _type = EQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:237:11: ( '=' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:237:16: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "NOTEQ"
    public final void mNOTEQ() throws RecognitionException {
        try {
            int _type = NOTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:238:11: ( '<>' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:238:16: '<>'
            {
            match("<>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NOTEQ"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:239:11: ( '-' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:239:16: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:240:11: ( '+' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:240:16: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "DIV"
    public final void mDIV() throws RecognitionException {
        try {
            int _type = DIV;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:241:11: ( '/' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:241:16: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DIV"

    // $ANTLR start "MULT"
    public final void mMULT() throws RecognitionException {
        try {
            int _type = MULT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:242:11: ( '*' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:242:16: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "MULT"

    // $ANTLR start "EXP"
    public final void mEXP() throws RecognitionException {
        try {
            int _type = EXP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:243:11: ( '^' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:243:16: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EXP"

    // $ANTLR start "COMMA"
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:244:11: ( ',' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:244:16: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "LPAREN"
    public final void mLPAREN() throws RecognitionException {
        try {
            int _type = LPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:245:11: ( '(' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:245:16: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LPAREN"

    // $ANTLR start "RPAREN"
    public final void mRPAREN() throws RecognitionException {
        try {
            int _type = RPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:246:11: ( ')' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:246:16: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RPAREN"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:249:11: ( 'INTEGER' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:249:16: 'INTEGER'
            {
            match("INTEGER"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "DBL"
    public final void mDBL() throws RecognitionException {
        try {
            int _type = DBL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:250:11: ( 'DOUBLE' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:250:16: 'DOUBLE'
            {
            match("DOUBLE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DBL"

    // $ANTLR start "STR"
    public final void mSTR() throws RecognitionException {
        try {
            int _type = STR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:251:11: ( 'STRING' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:251:16: 'STRING'
            {
            match("STRING"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "STR"

    // $ANTLR start "BLN"
    public final void mBLN() throws RecognitionException {
        try {
            int _type = BLN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:252:11: ( 'BOOLEAN' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:252:16: 'BOOLEAN'
            {
            match("BOOLEAN"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "BLN"

    // $ANTLR start "NL"
    public final void mNL() throws RecognitionException {
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:12: ( ( ( '\\r' )? '\\n' | '\\r' ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:14: ( ( '\\r' )? '\\n' | '\\r' )
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:14: ( ( '\\r' )? '\\n' | '\\r' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='\r') ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1=='\n') ) {
                    alt2=1;
                }
                else {
                    alt2=2;}
            }
            else if ( (LA2_0=='\n') ) {
                alt2=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:16: ( '\\r' )? '\\n'
                    {
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:16: ( '\\r' )?
                    int alt1=2;
                    int LA1_0 = input.LA(1);

                    if ( (LA1_0=='\r') ) {
                        alt1=1;
                    }
                    switch (alt1) {
                        case 1 :
                            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:16: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;
                case 2 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:260:29: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "NL"

    // $ANTLR start "LETTER"
    public final void mLETTER() throws RecognitionException {
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:261:17: ( ( 'a' .. 'z' | 'A' .. 'Z' ) )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:261:19: ( 'a' .. 'z' | 'A' .. 'Z' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "LETTER"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:262:16: ( '0' .. '9' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:262:18: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:263:8: ( REMARK ( options {greedy=false; } : . )* NL )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:263:10: REMARK ( options {greedy=false; } : . )* NL
            {
            mREMARK(); 
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:263:17: ( options {greedy=false; } : . )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='\r') ) {
                    alt3=2;
                }
                else if ( (LA3_0=='\n') ) {
                    alt3=2;
                }
                else if ( ((LA3_0>='\u0000' && LA3_0<='\t')||(LA3_0>='\u000B' && LA3_0<='\f')||(LA3_0>='\u000E' && LA3_0<='\uFFFF')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:263:42: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            mNL(); 
            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "BOOLEAN"
    public final void mBOOLEAN() throws RecognitionException {
        try {
            int _type = BOOLEAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:264:8: ( 'TRUE' | 'FALSE' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='T') ) {
                alt4=1;
            }
            else if ( (LA4_0=='F') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:264:10: 'TRUE'
                    {
                    match("TRUE"); 


                    }
                    break;
                case 2 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:264:19: 'FALSE'
                    {
                    match("FALSE"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "BOOLEAN"

    // $ANTLR start "INTEGER"
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:265:8: ( ( DIGIT )+ )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:265:10: ( DIGIT )+
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:265:10: ( DIGIT )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:265:10: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INTEGER"

    // $ANTLR start "DOUBLE"
    public final void mDOUBLE() throws RecognitionException {
        try {
            int _type = DOUBLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:7: ( INTEGER '.' ( INTEGER )* | '.' ( INTEGER )+ )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                alt8=1;
            }
            else if ( (LA8_0=='.') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:9: INTEGER '.' ( INTEGER )*
                    {
                    mINTEGER(); 
                    match('.'); 
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:21: ( INTEGER )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:21: INTEGER
                    	    {
                    	    mINTEGER(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:32: '.' ( INTEGER )+
                    {
                    match('.'); 
                    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:36: ( INTEGER )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:266:36: INTEGER
                    	    {
                    	    mINTEGER(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DOUBLE"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:267:3: ( LETTER ( LETTER | DIGIT )* )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:267:5: LETTER ( LETTER | DIGIT )*
            {
            mLETTER(); 
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:267:12: ( LETTER | DIGIT )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='0' && LA9_0<='9')||(LA9_0>='A' && LA9_0<='Z')||(LA9_0>='a' && LA9_0<='z')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:268:7: ( '\\\"' ( . )* '\\\"' )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:268:9: '\\\"' ( . )* '\\\"'
            {
            match('\"'); 
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:268:14: ( . )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0=='\"') ) {
                    alt10=2;
                }
                else if ( ((LA10_0>='\u0000' && LA10_0<='!')||(LA10_0>='#' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:268:14: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:269:3: ( ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' )+ )
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:269:5: ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' )+
            {
            // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:269:5: ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\t' && LA11_0<='\n')||(LA11_0>='\f' && LA11_0<='\r')||LA11_0==' ') ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||(input.LA(1)>='\f' && input.LA(1)<='\r')||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);

            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:8: ( AS | CALL | DECLARE | ELSE | END | FUNCTION | IF | MOD | PRINT | REMARK | RETURN | THEN | WHILE | LTEQ | LT | GTEQ | GT | EQ | NOTEQ | MINUS | PLUS | DIV | MULT | EXP | COMMA | LPAREN | RPAREN | INT | DBL | STR | BLN | COMMENT | BOOLEAN | INTEGER | DOUBLE | ID | STRING | WS )
        int alt12=38;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:10: AS
                {
                mAS(); 

                }
                break;
            case 2 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:13: CALL
                {
                mCALL(); 

                }
                break;
            case 3 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:18: DECLARE
                {
                mDECLARE(); 

                }
                break;
            case 4 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:26: ELSE
                {
                mELSE(); 

                }
                break;
            case 5 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:31: END
                {
                mEND(); 

                }
                break;
            case 6 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:35: FUNCTION
                {
                mFUNCTION(); 

                }
                break;
            case 7 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:44: IF
                {
                mIF(); 

                }
                break;
            case 8 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:47: MOD
                {
                mMOD(); 

                }
                break;
            case 9 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:51: PRINT
                {
                mPRINT(); 

                }
                break;
            case 10 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:57: REMARK
                {
                mREMARK(); 

                }
                break;
            case 11 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:64: RETURN
                {
                mRETURN(); 

                }
                break;
            case 12 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:71: THEN
                {
                mTHEN(); 

                }
                break;
            case 13 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:76: WHILE
                {
                mWHILE(); 

                }
                break;
            case 14 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:82: LTEQ
                {
                mLTEQ(); 

                }
                break;
            case 15 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:87: LT
                {
                mLT(); 

                }
                break;
            case 16 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:90: GTEQ
                {
                mGTEQ(); 

                }
                break;
            case 17 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:95: GT
                {
                mGT(); 

                }
                break;
            case 18 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:98: EQ
                {
                mEQ(); 

                }
                break;
            case 19 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:101: NOTEQ
                {
                mNOTEQ(); 

                }
                break;
            case 20 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:107: MINUS
                {
                mMINUS(); 

                }
                break;
            case 21 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:113: PLUS
                {
                mPLUS(); 

                }
                break;
            case 22 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:118: DIV
                {
                mDIV(); 

                }
                break;
            case 23 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:122: MULT
                {
                mMULT(); 

                }
                break;
            case 24 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:127: EXP
                {
                mEXP(); 

                }
                break;
            case 25 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:131: COMMA
                {
                mCOMMA(); 

                }
                break;
            case 26 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:137: LPAREN
                {
                mLPAREN(); 

                }
                break;
            case 27 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:144: RPAREN
                {
                mRPAREN(); 

                }
                break;
            case 28 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:151: INT
                {
                mINT(); 

                }
                break;
            case 29 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:155: DBL
                {
                mDBL(); 

                }
                break;
            case 30 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:159: STR
                {
                mSTR(); 

                }
                break;
            case 31 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:163: BLN
                {
                mBLN(); 

                }
                break;
            case 32 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:167: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 33 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:175: BOOLEAN
                {
                mBOOLEAN(); 

                }
                break;
            case 34 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:183: INTEGER
                {
                mINTEGER(); 

                }
                break;
            case 35 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:191: DOUBLE
                {
                mDOUBLE(); 

                }
                break;
            case 36 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:198: ID
                {
                mID(); 

                }
                break;
            case 37 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:201: STRING
                {
                mSTRING(); 

                }
                break;
            case 38 :
                // /home/davors/git/miniJVM/MiniJVM/src/Procedural.g:1:208: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA12_eotS =
        "\1\uffff\10\34\1\53\3\34\1\63\1\65\11\uffff\2\34\1\70\4\uffff\1"+
        "\71\7\34\1\101\3\34\2\uffff\4\34\5\uffff\2\34\2\uffff\4\34\1\117"+
        "\2\34\1\uffff\1\34\1\123\7\34\1\133\2\34\1\136\1\uffff\3\34\1\uffff"+
        "\2\34\1\144\1\145\3\34\1\uffff\2\34\1\uffff\1\34\1\145\1\34\1\155"+
        "\1\34\2\uffff\1\157\3\34\1\163\2\34\1\uffff\1\166\1\uffff\1\167"+
        "\1\34\1\171\1\uffff\1\34\1\173\2\uffff\1\174\1\uffff\1\175\3\uffff";
    static final String DFA12_eofS =
        "\176\uffff";
    static final String DFA12_minS =
        "\1\11\1\123\1\101\1\105\1\114\1\101\1\106\1\117\1\122\1\0\1\105"+
        "\2\110\2\75\11\uffff\1\124\1\117\1\56\4\uffff\1\60\1\114\1\103\1"+
        "\125\1\123\1\104\1\116\1\114\1\60\1\124\1\104\1\111\2\uffff\1\124"+
        "\1\105\1\125\1\111\5\uffff\1\122\1\117\2\uffff\2\114\1\102\1\105"+
        "\1\60\1\103\1\123\1\uffff\1\105\1\60\1\116\1\125\1\116\1\105\1\114"+
        "\1\111\1\114\1\60\1\101\1\114\1\60\1\uffff\1\124\1\105\1\107\1\uffff"+
        "\1\124\1\122\2\60\1\105\1\116\1\105\1\uffff\1\122\1\105\1\uffff"+
        "\1\111\1\60\1\105\1\60\1\116\2\uffff\1\60\1\107\1\101\1\105\1\60"+
        "\1\117\1\122\1\uffff\1\60\1\uffff\1\60\1\116\1\60\1\uffff\1\116"+
        "\1\60\2\uffff\1\60\1\uffff\1\60\3\uffff";
    static final String DFA12_maxS =
        "\1\172\1\123\1\101\1\117\1\116\1\125\1\116\1\117\1\122\1\uffff\1"+
        "\105\1\122\1\110\1\76\1\75\11\uffff\1\124\1\117\1\71\4\uffff\1\172"+
        "\1\114\1\103\1\125\1\123\1\104\1\116\1\114\1\172\1\124\1\104\1\111"+
        "\2\uffff\1\124\1\105\1\125\1\111\5\uffff\1\122\1\117\2\uffff\2\114"+
        "\1\102\1\105\1\172\1\103\1\123\1\uffff\1\105\1\172\1\116\1\125\1"+
        "\116\1\105\1\114\1\111\1\114\1\172\1\101\1\114\1\172\1\uffff\1\124"+
        "\1\105\1\107\1\uffff\1\124\1\122\2\172\1\105\1\116\1\105\1\uffff"+
        "\1\122\1\105\1\uffff\1\111\1\172\1\105\1\172\1\116\2\uffff\1\172"+
        "\1\107\1\101\1\105\1\172\1\117\1\122\1\uffff\1\172\1\uffff\1\172"+
        "\1\116\1\172\1\uffff\1\116\1\172\2\uffff\1\172\1\uffff\1\172\3\uffff";
    static final String DFA12_acceptS =
        "\17\uffff\1\22\1\24\1\25\1\26\1\27\1\30\1\31\1\32\1\33\3\uffff\1"+
        "\43\1\44\1\45\1\46\14\uffff\1\12\1\40\4\uffff\1\16\1\23\1\17\1\20"+
        "\1\21\2\uffff\1\42\1\1\7\uffff\1\7\15\uffff\1\5\3\uffff\1\10\7\uffff"+
        "\1\2\2\uffff\1\4\5\uffff\1\14\1\41\7\uffff\1\11\1\uffff\1\15\3\uffff"+
        "\1\35\2\uffff\1\13\1\36\1\uffff\1\3\1\uffff\1\34\1\37\1\6";
    static final String DFA12_specialS =
        "\11\uffff\1\0\164\uffff}>";
    static final String[] DFA12_transitionS = {
            "\2\36\1\uffff\2\36\22\uffff\1\36\1\uffff\1\35\1\11\4\uffff\1"+
            "\26\1\27\1\23\1\21\1\25\1\20\1\33\1\22\12\32\2\uffff\1\15\1"+
            "\17\1\16\2\uffff\1\1\1\31\1\2\1\3\1\4\1\5\2\34\1\6\3\34\1\7"+
            "\2\34\1\10\1\34\1\12\1\30\1\13\2\34\1\14\3\34\3\uffff\1\24\2"+
            "\uffff\32\34",
            "\1\37",
            "\1\40",
            "\1\41\11\uffff\1\42",
            "\1\43\1\uffff\1\44",
            "\1\46\23\uffff\1\45",
            "\1\47\7\uffff\1\50",
            "\1\51",
            "\1\52",
            "\0\54",
            "\1\55",
            "\1\56\11\uffff\1\57",
            "\1\60",
            "\1\61\1\62",
            "\1\64",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\66",
            "\1\67",
            "\1\33\1\uffff\12\32",
            "",
            "",
            "",
            "",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\75",
            "\1\76",
            "\1\77",
            "\1\100",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\102",
            "\1\103",
            "\1\104",
            "",
            "",
            "\1\105",
            "\1\106",
            "\1\107",
            "\1\110",
            "",
            "",
            "",
            "",
            "",
            "\1\111",
            "\1\112",
            "",
            "",
            "\1\113",
            "\1\114",
            "\1\115",
            "\1\116",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\120",
            "\1\121",
            "",
            "\1\122",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\124",
            "\1\125",
            "\1\126",
            "\1\127",
            "\1\130",
            "\1\131",
            "\1\132",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\134",
            "\1\135",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "",
            "\1\137",
            "\1\140",
            "\1\141",
            "",
            "\1\142",
            "\1\143",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\146",
            "\1\147",
            "\1\150",
            "",
            "\1\151",
            "\1\152",
            "",
            "\1\153",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\154",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\156",
            "",
            "",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\160",
            "\1\161",
            "\1\162",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\164",
            "\1\165",
            "",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "\1\170",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "",
            "\1\172",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "",
            "",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "",
            "\12\34\7\uffff\32\34\6\uffff\32\34",
            "",
            "",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( AS | CALL | DECLARE | ELSE | END | FUNCTION | IF | MOD | PRINT | REMARK | RETURN | THEN | WHILE | LTEQ | LT | GTEQ | GT | EQ | NOTEQ | MINUS | PLUS | DIV | MULT | EXP | COMMA | LPAREN | RPAREN | INT | DBL | STR | BLN | COMMENT | BOOLEAN | INTEGER | DOUBLE | ID | STRING | WS );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA12_9 = input.LA(1);

                        s = -1;
                        if ( ((LA12_9>='\u0000' && LA12_9<='\uFFFF')) ) {s = 44;}

                        else s = 43;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 12, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}